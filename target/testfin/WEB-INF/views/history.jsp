<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>

<head>
    <title>История состояний</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<c:url value="/resourses/css/bootstrap.min.css" />" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="<c:url value="/resourses/js/bootstrap.min.js" />"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#"><img src="<c:url value="/resourses/img/logo2.png" />" width="93" height="30.5"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Просмотр истории рассчетов </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Справка</a>
            </li>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/admin">Страница администратора</a>
                </li>
            </sec:authorize>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>
</body>
<div class="card">
    <div class="card-header">
        <h5>Результаты рассчетов</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Дата</th>
                <th scope="col">y1</th>
                <th scope="col">y2</th>
                <th scope="col">y3</th>
                <th scope="col">y4</th>
                <th scope="col">y5</th>
                <th scope="col">z1</th>
                <th scope="col">z2</th>
                <th scope="col">z3</th>
                <th scope="col">z4</th>
                <th scope="col">z5</th>
                <th scope="col">Результат</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="h" items="${history}" >
            <tr>
                <td>${h.date} at ${h.time}</td>
                <td>${h.y1}</td>
                <td>${h.y2}</td>
                <td>${h.y3}</td>
                <td>${h.y4}</td>
                <td>${h.y5}</td>
                <td>${h.z1}</td>
                <td>${h.z2}</td>
                <td>${h.z3}</td>
                <td>${h.z4}</td>
                <td>${h.z5}</td>
                <td>${h.resultFz}</td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card card-body">
        <h3 class="text-center">График изменения сосотояния</h3>
    </div>
</div>
<script>
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Храктеристика филиала"
            },
            axisX:{

                valueFormatString: "DD MMM",
                crosshair: {
                    enabled: true,
                    snapToDataPoint: true
                }
            },
            axisY: {
                title: "Состояние",

            },
            toolTip:{
                shared:true
            },
            legend:{
                cursor:"pointer",
                verticalAlign: "bottom",
                horizontalAlign: "left",
                dockInsidePlotArea: true,
                itemclick: toogleDataSeries
            },
            data: [{
                type: "line",
                showInLegend: true,
                name: "Состояние",
                markerType: "square",
                xValueFormatString: "DD MMM, YYYY",
                color: "#F08080",
                dataPoints: [
                    <c:forEach var="h" items="${history}" >
                    <c:choose>
                        <c:when test="${h.resultFz.equals('CRITICAL')}">
                            { x: new Date(${h.year}, ${h.month}, ${h.day}), y: 1, indexLabel: "очень плохо", markerType: "cross", markerColor: "red" },
                        </c:when>
                        <c:when test="${h.resultFz.equals('BAD')}">
                            { x: new Date(${h.year}, ${h.month}, ${h.day}), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                        </c:when>
                        <c:when test="${h.resultFz.equals('NORMAL')}">
                            { x: new Date(${h.year}, ${h.month}, ${h.day}), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                        </c:when>
                        <c:when test="${h.resultFz.equals('GOOD')}">
                            { x: new Date(${h.year}, ${h.month}, ${h.day}), y: 4, indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                        </c:when>
                        <c:otherwise>
                            { x: new Date(${h.year}, ${h.month}, ${h.day}), y: 5, indexLabel: "хорошо", markerType: "triangle", markerColor: "green" },
                        </c:otherwise>
                    </c:choose>


                   // { x: new Date(2017, 0, 12), y: 1, indexLabel: "плохо", markerType: "cross", markerColor: "red" },
                   // { x: new Date(2017, 6, 12), y: 1, indexLabel: "очень плохо", markerType: "cross", markerColor: "red" },
                   // { x: new Date(2017, 10, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                  //  { x: new Date(2018, 0, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                  //  { x: new Date(2018, 1, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                  //  { x: new Date(2018, 2, 12), y: 1, indexLabel: "очень плохо", markerType: "cross", markerColor: "red" },
                   // { x: new Date(2018, 4, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato"},
                  //  { x: new Date(2018, 5, 15), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                   // { x: new Date(2018, 6, 16), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                   // { x: new Date(2018, 7, 13), y: 4, indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                   // { x: new Date(2018, 8, 11), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                  //  { x: new Date(2018, 10, 12), y: 4, indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                  //  { x: new Date(2019, 0, 16), y: 4,  indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                   // { x: new Date(2019, 2, 11), y: 5,  indexLabel: "очень хорошо", markerType: "triangle", markerColor: "green" },
                    </c:forEach>
                    { x: new Date(), y: ""}

                ]
            }]
        });
        chart.render();

        function toogleDataSeries(e){
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else{
                e.dataSeries.visible = true;
            }
            chart.render();
        }

    }
</script>

<div id="chartContainer" style="height: 500px; max-width: 1280px; margin: 0px auto;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</html>