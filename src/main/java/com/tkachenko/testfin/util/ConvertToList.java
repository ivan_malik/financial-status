package com.tkachenko.testfin.util;


import com.tkachenko.testfin.model.IndicatorsEntity;

import java.util.ArrayList;

public class ConvertToList {

    public static ArrayList<Double> getListFromIndicators(IndicatorsEntity indicatorsEntity) {
        ArrayList<Double> arrayList = new ArrayList<>();
        arrayList.add(indicatorsEntity.getX1());
        arrayList.add(indicatorsEntity.getX2());
        arrayList.add(indicatorsEntity.getX3());
        arrayList.add(indicatorsEntity.getX4());
        arrayList.add(indicatorsEntity.getX5());
        arrayList.add(indicatorsEntity.getX6());
        arrayList.add(indicatorsEntity.getX7());
        arrayList.add(indicatorsEntity.getX8());
        arrayList.add(indicatorsEntity.getX9());
        arrayList.add(indicatorsEntity.getX10());
        arrayList.add(indicatorsEntity.getX11());
        arrayList.add(indicatorsEntity.getX12());
        arrayList.add(indicatorsEntity.getX13());
        arrayList.add(indicatorsEntity.getX14());
        arrayList.add(indicatorsEntity.getX15());
        arrayList.add(indicatorsEntity.getX16());
        arrayList.add(indicatorsEntity.getX17());
        arrayList.add(indicatorsEntity.getX18());
        arrayList.add(indicatorsEntity.getX19());
        arrayList.add(indicatorsEntity.getX20());
        arrayList.add(indicatorsEntity.getX21());
        arrayList.add(indicatorsEntity.getX22());
        arrayList.add(indicatorsEntity.getX23());
        arrayList.add(indicatorsEntity.getX24());
        arrayList.add(indicatorsEntity.getX25());
        arrayList.add(indicatorsEntity.getX26());
        arrayList.add(indicatorsEntity.getX27());
        arrayList.add(indicatorsEntity.getX28());
        arrayList.add(indicatorsEntity.getX29());
        arrayList.add(indicatorsEntity.getX30());

        return arrayList;
    }
}
