package com.tkachenko.testfin.util;


import com.tkachenko.testfin.fuzzy.FinancialCoefficients;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class CreateReport {

    private static NumberFormat formatter = new DecimalFormat("#0.000");

    public static void modifyExistingWorkbook(FinancialCoefficients financialCoefficients, ArrayList<Double> params, String fileIN, String fileOut) throws InvalidFormatException, IOException {
        Workbook workbook = WorkbookFactory.create(new File(fileIN));
        Sheet sheet = workbook.getSheetAt(0);

        //y1
        Row row = sheet.getRow(2);
        Cell cell = row.getCell(0);
        cell.setCellValue(formatter.format(financialCoefficients.getY1()));

        //y2
        row = sheet.getRow(2);
        cell = row.getCell(1);
        cell.setCellValue(formatter.format(financialCoefficients.getY2()));

        //y3
        row = sheet.getRow(2);
        cell = row.getCell(2);
        cell.setCellValue(financialCoefficients.getY3());

        //y4
        row = sheet.getRow(5);
        cell = row.getCell(0);
        cell.setCellValue(financialCoefficients.getY4());

        //y5
        row = sheet.getRow(5);
        cell = row.getCell(1);
        cell.setCellValue(financialCoefficients.getY5());

        //y6
        row = sheet.getRow(5);
        cell = row.getCell(2);
        cell.setCellValue(financialCoefficients.getY6());

        //y7
        row = sheet.getRow(5);
        cell = row.getCell(3);
        cell.setCellValue(financialCoefficients.getY7());

        //y8
        row = sheet.getRow(5);
        cell = row.getCell(4);
        cell.setCellValue(financialCoefficients.getY8());

        //y9
        row = sheet.getRow(8);
        cell = row.getCell(0);
        cell.setCellValue(financialCoefficients.getY9());

        //y10
        row = sheet.getRow(8);
        cell = row.getCell(1);
        cell.setCellValue(financialCoefficients.getY10());

        //y11
        row = sheet.getRow(8);
        cell = row.getCell(2);
        cell.setCellValue(financialCoefficients.getY11());

        //y12
        row = sheet.getRow(8);
        cell = row.getCell(3);
        cell.setCellValue(financialCoefficients.getY12());

        //y13
        row = sheet.getRow(11);
        cell = row.getCell(0);
        cell.setCellValue(financialCoefficients.getY13());

        //y14
        row = sheet.getRow(11);
        cell = row.getCell(1);
        cell.setCellValue(financialCoefficients.getY14());

        //y15
        row = sheet.getRow(11);
        cell = row.getCell(2);
        cell.setCellValue(financialCoefficients.getY15());

        //y16
        row = sheet.getRow(11);
        cell = row.getCell(3);
        cell.setCellValue(financialCoefficients.getY16());

        //y17
        row = sheet.getRow(14);
        cell = row.getCell(0);
        cell.setCellValue(params.get(6));

        //y18
        row = sheet.getRow(14);
        cell = row.getCell(2);
        cell.setCellValue(params.get(7));

        //z1
        row = sheet.getRow(17);
        cell = row.getCell(0);
        cell.setCellValue(params.get(2));

        //z2
        row = sheet.getRow(17);
        cell = row.getCell(1);
        cell.setCellValue(params.get(3));

        //z3
        row = sheet.getRow(17);
        cell = row.getCell(2);
        cell.setCellValue(params.get(4));

        //z4
        row = sheet.getRow(17);
        cell = row.getCell(3);
        cell.setCellValue(params.get(5));

        //z5
        row = sheet.getRow(17);
        cell = row.getCell(4);
        cell.setCellValue(params.get(8));

        //classic
        row = sheet.getRow(20);
        cell = row.getCell(1);
        cell.setCellValue(params.get(0));

        //classic
        row = sheet.getRow(20);
        cell = row.getCell(2);
        cell.setCellValue(params.get(1));

        // Write the output to the file
        FileOutputStream newFile = new FileOutputStream(fileOut);
        workbook.write(newFile);
        newFile.close();

        // Closing the workbook
        workbook.close();
    }
}
