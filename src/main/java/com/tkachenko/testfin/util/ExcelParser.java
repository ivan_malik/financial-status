package com.tkachenko.testfin.util;


import com.tkachenko.testfin.model.IndicatorsEntity;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class ExcelParser {

    public static IndicatorsEntity getIndicators(String filePath) throws IOException, InvalidFormatException, NumberFormatException {
        IndicatorsEntity indicatorsEntity = new IndicatorsEntity();

        Workbook workbook = WorkbookFactory.create(new File(filePath));

        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        indicatorsEntity.setX1(getCell(48,7,sheet, dataFormatter));
        indicatorsEntity.setX2(getCell(47,7,sheet,dataFormatter));
        indicatorsEntity.setX3(getCell(100,7,sheet,dataFormatter));
        indicatorsEntity.setX4(getCell(97,7,sheet,dataFormatter));
        indicatorsEntity.setX5(getCell(114,7,sheet,dataFormatter));
        indicatorsEntity.setX6(getCell(59,7,sheet,dataFormatter));
        indicatorsEntity.setX7(getCell(59,9,sheet,dataFormatter));
        indicatorsEntity.setX8(getCell(28,7,sheet,dataFormatter));
        indicatorsEntity.setX9(getCell(76,7,sheet,dataFormatter));
        indicatorsEntity.setX10(getCell(76,9,sheet,dataFormatter));
        indicatorsEntity.setX11(getCell(61,7,sheet,dataFormatter));
        indicatorsEntity.setX12(getCell(61,9,sheet,dataFormatter));
        indicatorsEntity.setX13(getCell(95,7,sheet,dataFormatter));
        indicatorsEntity.setX14(getCell(115,7,sheet,dataFormatter));
        indicatorsEntity.setX15(getCell(38,7,sheet,dataFormatter));
        indicatorsEntity.setX16(getCell(107,7,sheet,dataFormatter));
        indicatorsEntity.setX17(getCell(111,7,sheet,dataFormatter));
        indicatorsEntity.setX18(getCell(113,7,sheet,dataFormatter));
        indicatorsEntity.setX19(getCell(118,7,sheet,dataFormatter));
        indicatorsEntity.setX20(getCell(133,6,sheet,dataFormatter));
        indicatorsEntity.setX21(getCell(124,6,sheet,dataFormatter));
        indicatorsEntity.setX22(getCell(144,6,sheet,dataFormatter));
        indicatorsEntity.setX23(getCell(160,6,sheet,dataFormatter));
        indicatorsEntity.setX24(getCell(30,7,sheet,dataFormatter));
        indicatorsEntity.setX25(getCell(30,9,sheet,dataFormatter));
        indicatorsEntity.setX26(getCell(130,6,sheet,dataFormatter));

        return indicatorsEntity;
    }

    private static Double getCell(int needRow, int needCell, Sheet sheet, DataFormatter dataFormatter) {
        int rows = 0;
        String cellValue = "";
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            int cells = 0;
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (rows == (needRow - 1) && cells == (needCell - 1) ) {
                    cellValue = dataFormatter.formatCellValue(cell);
                    break;
                }
                cells++;
            }
            rows++;
        }
        return Double.parseDouble(cellValue.trim().replace(" ","").replace(",","."));
    }
}
