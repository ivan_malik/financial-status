package com.tkachenko.testfin.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
            .withUser("user").password("{noop}password").roles("USER").and()
            .withUser("admin2").password("{noop}12345").roles("USER", "ADMIN");

        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select login, password, enabled from mydb.department where login=?")
                .authoritiesByUsernameQuery("select login, role from mydb.departments_roles where login=?")
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       /* http.authorizeRequests()
            .antMatchers("/admin/**")
                .access("hasRole('ADMIN')")
            .antMatchers("/user")
                .access("hasRole('USER') or hasRole('ADMIN')")
            .and().formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("login").passwordParameter("password").permitAll()
            .and().rememberMe().rememberMeParameter("remember_me").tokenValiditySeconds(2419200).key("rememberMeKey")
            .and().logout().logoutSuccessUrl("/login").and().exceptionHandling().accessDeniedPage("/Access_Denied")
            .and().httpBasic();

        http.authorizeRequests()
                .antMatchers("/admin/**")
                .access("hasRole('ADMIN')")
        .and().formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/login?error")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .permitAll()
        .and().logout()
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true);
        */
        http.authorizeRequests()
                .antMatchers("/admin/**")
                    .access("hasRole('ROLE_ADMIN')")
                .antMatchers("/**").access("hasRole('ROLE_USER')or hasRole('ROLE_ADMIN')")
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/j_spring_security_check")
                    .failureUrl("/login?error").permitAll()
                    .usernameParameter("username").passwordParameter("password")
                .and()
                    .logout()
                    .logoutUrl("/j_spring_security_logout")
                    .logoutSuccessUrl("/login?logout")
                .and()
                    .exceptionHandling()
                    .accessDeniedPage("/403")
                .and()
                    .csrf();
    }
}
