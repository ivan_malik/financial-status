package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.DepartmentEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class DepartmentDao {

    @Autowired
    private SessionFactory sessionFactory;

    public DepartmentDao() {
    }

    public DepartmentDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public DepartmentEntity findByLogin(String login) {

        DepartmentEntity departmentEntity  = null;
        departmentEntity = (DepartmentEntity) sessionFactory.getCurrentSession().get(DepartmentEntity.class, login);
        Hibernate.initialize(departmentEntity);
        return departmentEntity;
    }

    @Transactional
    public void save(DepartmentEntity departmentEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(departmentEntity);
    }

    @Transactional
    public void deleteByLogin(String login) {
        DepartmentEntity departmentEntity = new DepartmentEntity();
        departmentEntity.setLogin(login);
        sessionFactory.getCurrentSession().delete(departmentEntity);
    }

    @Transactional
    public void delete(DepartmentEntity departmentEntity) {
        sessionFactory.getCurrentSession().delete(departmentEntity);
    }


}
