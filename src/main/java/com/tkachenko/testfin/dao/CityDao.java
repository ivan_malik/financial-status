package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.CityEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class CityDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public CityEntity findById(int id) {

        CityEntity cityEntity  = null;
        cityEntity = (CityEntity) sessionFactory.getCurrentSession().get(CityEntity.class, id);
        Hibernate.initialize(cityEntity);
        return cityEntity;
    }

    @Transactional
    public void save(CityEntity cityEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(cityEntity);
    }

    @Transactional
    public void deleteById(int id) {
        CityEntity cityEntity = new CityEntity();
        cityEntity.setCityId(id);
        sessionFactory.getCurrentSession().delete(cityEntity);
    }

    @Transactional
    public void delete(CityEntity cityEntity) {
        sessionFactory.getCurrentSession().delete(cityEntity);
    }
}
