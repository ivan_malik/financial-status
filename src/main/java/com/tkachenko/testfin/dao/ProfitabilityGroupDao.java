package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.ProfitabilityGroupEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class ProfitabilityGroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public ProfitabilityGroupEntity findById(int id) {

        ProfitabilityGroupEntity profitabilityGroupEntity  = null;
        profitabilityGroupEntity = (ProfitabilityGroupEntity) sessionFactory.getCurrentSession().get(ProfitabilityGroupEntity.class, id);
        Hibernate.initialize(profitabilityGroupEntity);
        return profitabilityGroupEntity;
    }

    @Transactional
    public void save(ProfitabilityGroupEntity profitabilityGroupEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(profitabilityGroupEntity);
    }

    @Transactional
    public void deleteById(int id) {
        ProfitabilityGroupEntity profitabilityGroupEntity = new ProfitabilityGroupEntity();
        profitabilityGroupEntity.setProfitabilityGroupId(id);
        sessionFactory.getCurrentSession().delete(profitabilityGroupEntity);
    }

    @Transactional
    public void delete(ProfitabilityGroupEntity profitabilityGroupEntity) {
        sessionFactory.getCurrentSession().delete(profitabilityGroupEntity);
    }

    @Transactional
    public ProfitabilityGroupEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<ProfitabilityGroupEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from ProfitabilityGroupEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }
}
