package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.RecvalProfgEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class RecvalProfgDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public RecvalProfgEntity findById(int id) {

        RecvalProfgEntity recvalProfgEntity  = null;
        recvalProfgEntity = (RecvalProfgEntity) sessionFactory.getCurrentSession().get(RecvalProfgEntity.class, id);
        Hibernate.initialize(recvalProfgEntity);
        return recvalProfgEntity;
    }

    @Transactional
    public void save(RecvalProfgEntity recvalProfgEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(recvalProfgEntity);
    }

    @Transactional
    public void deleteById(int id) {
        RecvalProfgEntity recvalProfgEntity = new RecvalProfgEntity();
        recvalProfgEntity.setRecvalProfgId(id);
        sessionFactory.getCurrentSession().delete(recvalProfgEntity);
    }

    @Transactional
    public void delete(RecvalProfgEntity recvalProfgEntity) {
        sessionFactory.getCurrentSession().delete(recvalProfgEntity);
    }

    @Transactional
    public RecvalProfgEntity findWithMaxDate() {
        @SuppressWarnings("unchecked")
        TypedQuery<RecvalProfgEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from RecvalProfgEntity where updateDate IN (SELECT MAX(updateDate) from RecvalProfgEntity)");

        return query.getSingleResult();
    }
}
