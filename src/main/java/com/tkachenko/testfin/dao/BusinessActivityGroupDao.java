package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.BusinessActivityGroupEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class BusinessActivityGroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public BusinessActivityGroupEntity findById(int id) {

        BusinessActivityGroupEntity businessActivityGroupEntity  = null;
        businessActivityGroupEntity = (BusinessActivityGroupEntity) sessionFactory.getCurrentSession().get(BusinessActivityGroupEntity.class, id);
        Hibernate.initialize(businessActivityGroupEntity);
        return businessActivityGroupEntity;
    }

    @Transactional
    public void save(BusinessActivityGroupEntity businessActivityGroupEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(businessActivityGroupEntity);
    }

    @Transactional
    public void deleteById(int id) {
        BusinessActivityGroupEntity businessActivityGroupEntity = new BusinessActivityGroupEntity();
        businessActivityGroupEntity.setBusinessActivityGroupId(id);
        sessionFactory.getCurrentSession().delete(businessActivityGroupEntity);
    }

    @Transactional
    public void delete(BusinessActivityGroupEntity businessActivityGroupEntity) {
        sessionFactory.getCurrentSession().delete(businessActivityGroupEntity);
    }

    @Transactional
    public BusinessActivityGroupEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<BusinessActivityGroupEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from BusinessActivityGroupEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }
}
