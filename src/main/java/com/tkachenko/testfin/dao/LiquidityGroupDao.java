package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.LiquidityGroupEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class LiquidityGroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public LiquidityGroupEntity findById(int id) {

        LiquidityGroupEntity liquidityGroupEntity  = null;
        liquidityGroupEntity = (LiquidityGroupEntity) sessionFactory.getCurrentSession().get(LiquidityGroupEntity.class, id);
        Hibernate.initialize(liquidityGroupEntity);
        return liquidityGroupEntity;
    }

    @Transactional
    public void save(LiquidityGroupEntity liquidityGroupEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(liquidityGroupEntity);
    }

    @Transactional
    public void deleteById(int id) {
        LiquidityGroupEntity liquidityGroupEntity = new LiquidityGroupEntity();
        liquidityGroupEntity.setLiquidityGroupId(id);
        sessionFactory.getCurrentSession().delete(liquidityGroupEntity);
    }

    @Transactional
    public void delete(LiquidityGroupEntity liquidityGroupEntity) {
        sessionFactory.getCurrentSession().delete(liquidityGroupEntity);
    }

    @Transactional
    public LiquidityGroupEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<LiquidityGroupEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from LiquidityGroupEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }
}
