package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.RecvalQualgEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class RecvalQualgDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public RecvalQualgEntity findById(int id) {

        RecvalQualgEntity recvalQualgEntity  = null;
        recvalQualgEntity = (RecvalQualgEntity) sessionFactory.getCurrentSession().get(RecvalQualgEntity.class, id);
        Hibernate.initialize(recvalQualgEntity);
        return recvalQualgEntity;
    }

    @Transactional
    public void save(RecvalQualgEntity recvalQualgEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(recvalQualgEntity);
    }

    @Transactional
    public void deleteById(int id) {
        RecvalQualgEntity recvalQualgEntity = new RecvalQualgEntity();
        recvalQualgEntity.setRecvalQualgId(id);
        sessionFactory.getCurrentSession().delete(recvalQualgEntity);
    }

    @Transactional
    public void delete(RecvalQualgEntity recvalQualgEntity) {
        sessionFactory.getCurrentSession().delete(recvalQualgEntity);
    }

    @Transactional
    public RecvalQualgEntity findWithMaxDate() {
        @SuppressWarnings("unchecked")
        TypedQuery<RecvalQualgEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from RecvalQualgEntity where updateDate IN (SELECT MAX(updateDate) from RecvalQualgEntity)");

        return query.getSingleResult();
    }


}
