package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.QualitativeGroupEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class QualitativeGroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public QualitativeGroupEntity findById(int id) {

        QualitativeGroupEntity qualitativeGroupEntity  = null;
        qualitativeGroupEntity = (QualitativeGroupEntity) sessionFactory.getCurrentSession().get(QualitativeGroupEntity.class, id);
        Hibernate.initialize(qualitativeGroupEntity);
        return qualitativeGroupEntity;
    }

    @Transactional
    public void save(QualitativeGroupEntity qualitativeGroupEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(qualitativeGroupEntity);
    }

    @Transactional
    public void deleteById(int id) {
        QualitativeGroupEntity qualitativeGroupEntity = new QualitativeGroupEntity();
        qualitativeGroupEntity.setQualitativeGroupId(id);
        sessionFactory.getCurrentSession().delete(qualitativeGroupEntity);
    }

    @Transactional
    public void delete(QualitativeGroupEntity qualitativeGroupEntity) {
        sessionFactory.getCurrentSession().delete(qualitativeGroupEntity);
    }

    @Transactional
    public QualitativeGroupEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<QualitativeGroupEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from QualitativeGroupEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }
}
