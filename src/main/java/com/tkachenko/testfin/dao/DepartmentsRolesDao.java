package com.tkachenko.testfin.dao;


import com.tkachenko.testfin.model.DepartmentsRolesEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class DepartmentsRolesDao {

    @Autowired
    private SessionFactory sessionFactory;

    public DepartmentsRolesDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public DepartmentsRolesDao() {
    }

    @Transactional
    public DepartmentsRolesEntity findById(int id) {

        DepartmentsRolesEntity departmentsRolesEntity  = null;
        departmentsRolesEntity = (DepartmentsRolesEntity) sessionFactory.getCurrentSession().get(DepartmentsRolesEntity.class, id);
        Hibernate.initialize(departmentsRolesEntity);
        return departmentsRolesEntity;
    }

    @Transactional
    public void save(DepartmentsRolesEntity departmentsRolesEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(departmentsRolesEntity);
    }

    @Transactional
    public void deleteById(int id) {
        DepartmentsRolesEntity departmentsRolesEntity = new DepartmentsRolesEntity();
        departmentsRolesEntity.setDepartmentRoleId(id);
        sessionFactory.getCurrentSession().delete(departmentsRolesEntity);
    }

    @Transactional
    public void delete(DepartmentsRolesEntity departmentsRolesEntity) {
        sessionFactory.getCurrentSession().delete(departmentsRolesEntity);
    }
}
