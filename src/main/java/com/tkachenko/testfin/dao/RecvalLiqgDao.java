package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.RecvalLiqgEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class RecvalLiqgDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public RecvalLiqgEntity findById(int id) {

        RecvalLiqgEntity recvalLiqgEntity  = null;
        recvalLiqgEntity = (RecvalLiqgEntity) sessionFactory.getCurrentSession().get(RecvalLiqgEntity.class, id);
        Hibernate.initialize(recvalLiqgEntity);
        return recvalLiqgEntity;
    }

    @Transactional
    public void save(RecvalLiqgEntity recvalLiqgEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(recvalLiqgEntity);
    }

    @Transactional
    public void deleteById(int id) {
        RecvalLiqgEntity recvalLiqgEntity = new RecvalLiqgEntity();
        recvalLiqgEntity.setIdRecvalLg(id);
        sessionFactory.getCurrentSession().delete(recvalLiqgEntity);
    }

    @Transactional
    public void delete(RecvalLiqgEntity recvalLiqgEntity) {
        sessionFactory.getCurrentSession().delete(recvalLiqgEntity);
    }

    @Transactional
    public RecvalLiqgEntity findWithMaxDate() {
        @SuppressWarnings("unchecked")
        TypedQuery<RecvalLiqgEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from RecvalLiqgEntity where updateDate IN (SELECT MAX(updateDate) from RecvalLiqgEntity)");

        return query.getSingleResult();
    }
}
