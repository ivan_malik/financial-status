package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.RecvalFsgEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class RecvalFsgDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public RecvalFsgEntity findById(int id) {

        RecvalFsgEntity recvalFsgEntity  = null;
        recvalFsgEntity = (RecvalFsgEntity) sessionFactory.getCurrentSession().get(RecvalFsgEntity.class, id);
        Hibernate.initialize(recvalFsgEntity);
        return recvalFsgEntity;
    }

    @Transactional
    public void save(RecvalFsgEntity recvalFsgEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(recvalFsgEntity);
    }

    @Transactional
    public void deleteById(int id) {
        RecvalFsgEntity recvalFsgEntity = new RecvalFsgEntity();
        recvalFsgEntity.setRecvalFsgId(id);
        sessionFactory.getCurrentSession().delete(recvalFsgEntity);
    }

    @Transactional
    public void delete(RecvalFsgEntity recvalFsgEntity) {
        sessionFactory.getCurrentSession().delete(recvalFsgEntity);
    }

    @Transactional
    public RecvalFsgEntity findWithMaxDate() {
        @SuppressWarnings("unchecked")
        TypedQuery<RecvalFsgEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from RecvalFsgEntity where updateDate IN (SELECT MAX(updateDate) from RecvalFsgEntity)");

        return query.getSingleResult();
    }
}
