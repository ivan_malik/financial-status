package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.RecvalBagEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class RecvalBagDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public RecvalBagEntity findById(int id) {

        RecvalBagEntity recvalBagEntity  = null;
        recvalBagEntity = (RecvalBagEntity) sessionFactory.getCurrentSession().get(RecvalBagEntity.class, id);
        Hibernate.initialize(recvalBagEntity);
        return recvalBagEntity;
    }

    @Transactional
    public void save(RecvalBagEntity recvalBagEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(recvalBagEntity);
    }

    @Transactional
    public void deleteById(int id) {
        RecvalBagEntity recvalBagEntity = new RecvalBagEntity();
        recvalBagEntity.setRecvalBagId(id);
        sessionFactory.getCurrentSession().delete(recvalBagEntity);
    }

    @Transactional
    public void delete(RecvalBagEntity recvalBagEntity) {
        sessionFactory.getCurrentSession().delete(recvalBagEntity);
    }

    @Transactional
    public RecvalBagEntity findWithMaxDate() {
        @SuppressWarnings("unchecked")
        TypedQuery<RecvalBagEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from RecvalBagEntity where updateDate IN (SELECT MAX(updateDate) from RecvalBagEntity)");

        return query.getSingleResult();
    }
}
