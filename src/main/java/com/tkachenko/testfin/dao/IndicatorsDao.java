package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.DepartmentEntity;
import com.tkachenko.testfin.model.IndicatorsEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Repository
public class IndicatorsDao {

    @Autowired
    private SessionFactory sessionFactory;

    public IndicatorsDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public IndicatorsDao() {
    }

    @Transactional
    public IndicatorsEntity findById(int id) {

        IndicatorsEntity indicatorsEntity  = null;
        indicatorsEntity = (IndicatorsEntity) sessionFactory.getCurrentSession().get(IndicatorsEntity.class, id);
        Hibernate.initialize(indicatorsEntity);
        return indicatorsEntity;
    }

    @Transactional
    public List<IndicatorsEntity> findByDate(Date date, DepartmentEntity login) {
        @SuppressWarnings("unchecked")
        TypedQuery<IndicatorsEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from IndicatorsEntity i where i.calculationDate = :date and i.department = :login");
        query.setParameter("date", date);
        query.setParameter("login", login);
        List<IndicatorsEntity> list = query.getResultList();

        return list;
    }


    @Transactional
    public void save(IndicatorsEntity indicatorsEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(indicatorsEntity);
    }

    @Transactional
    public void deleteById(int id) {
        IndicatorsEntity indicatorsEntity = new IndicatorsEntity();
        indicatorsEntity.setIdindicatorsId(id);
        sessionFactory.getCurrentSession().delete(indicatorsEntity);
    }

    @Transactional
    public void delete(IndicatorsEntity indicatorsEntitys) {
        sessionFactory.getCurrentSession().delete(indicatorsEntitys);
    }

    @Transactional
    public List<IndicatorsEntity> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<IndicatorsEntity> query = sessionFactory.getCurrentSession().createQuery("from IndicatorsEntity");
        return query.getResultList();
    }

    @Transactional
    public List<IndicatorsEntity> findAllForDepartment(String login) {
        @SuppressWarnings("unchecked")
        TypedQuery<IndicatorsEntity> query = sessionFactory.getCurrentSession().createQuery("from IndicatorsEntity where department.login =:login");
        query.setParameter("login", login);
        return query.getResultList();
    }


}
