package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.CountryEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class CountryDao {

    @Autowired
    private SessionFactory sessionFactory;

    public CountryDao() {
    }

    public CountryDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public CountryEntity findById(int id) {

        CountryEntity countryEntity  = null;
        countryEntity = (CountryEntity) sessionFactory.getCurrentSession().get(CountryEntity.class, id);
        Hibernate.initialize(countryEntity);
        return countryEntity;
    }

    @Transactional
    public void save(CountryEntity countryEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(countryEntity);
    }

    @Transactional
    public void deleteById(int id) {
        CountryEntity countryEntity = new CountryEntity();
        countryEntity.setCountryId(id);
        sessionFactory.getCurrentSession().delete(countryEntity);
    }

    @Transactional
    public void delete(CountryEntity countryEntity) {
        sessionFactory.getCurrentSession().delete(countryEntity);
    }

}
