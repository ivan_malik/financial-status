package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.FinancialStabilityGroupEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class FinancialStabilityGroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public FinancialStabilityGroupEntity findById(int id) {

        FinancialStabilityGroupEntity financialStabilityGroupEntity  = null;
        financialStabilityGroupEntity = (FinancialStabilityGroupEntity) sessionFactory.getCurrentSession().get(FinancialStabilityGroupEntity.class, id);
        Hibernate.initialize(financialStabilityGroupEntity);
        return financialStabilityGroupEntity;
    }

    @Transactional
    public void save(FinancialStabilityGroupEntity financialStabilityGroupEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(financialStabilityGroupEntity);
    }

    @Transactional
    public void deleteById(int id) {
        FinancialStabilityGroupEntity financialStabilityGroupEntity = new FinancialStabilityGroupEntity();
        financialStabilityGroupEntity.setFinancialStabilityGroupId(id);
        sessionFactory.getCurrentSession().delete(financialStabilityGroupEntity);
    }

    @Transactional
    public void delete(FinancialStabilityGroupEntity financialStabilityGroupEntity) {
        sessionFactory.getCurrentSession().delete(financialStabilityGroupEntity);
    }

    @Transactional
    public FinancialStabilityGroupEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<FinancialStabilityGroupEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from FinancialStabilityGroupEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }
}
