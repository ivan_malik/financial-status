package com.tkachenko.testfin.dao;

import com.tkachenko.testfin.model.ResultsEntity;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class ResultDao {

    @Autowired
    private SessionFactory sessionFactory;

    public ResultDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public ResultDao() {
    }

    @Transactional
    public ResultsEntity findById(int id) {

        ResultsEntity resultsEntity  = null;
        resultsEntity = (ResultsEntity) sessionFactory.getCurrentSession().get(ResultsEntity.class, id);
        Hibernate.initialize(resultsEntity);
        return resultsEntity;
    }

    @Transactional
    public void save(ResultsEntity resultsEntity) {
        sessionFactory.getCurrentSession().saveOrUpdate(resultsEntity);
    }

    @Transactional
    public void deleteById(int id) {
        ResultsEntity resultsEntity = new ResultsEntity();
        resultsEntity.setResultsId(id);
        sessionFactory.getCurrentSession().delete(resultsEntity);
    }

    @Transactional
    public void delete(ResultsEntity resultsEntity) {
        sessionFactory.getCurrentSession().delete(resultsEntity);
    }

    @Transactional
    public ResultsEntity findAllByIndicators(int indicatorsId) {

        @SuppressWarnings("unchecked")
        TypedQuery<ResultsEntity> query = sessionFactory.getCurrentSession()
                .createQuery("from ResultsEntity l where l.indicatorsEntity.idindicatorsId = :id");
        query.setParameter("id", indicatorsId);

        return query.getSingleResult();
    }

}
