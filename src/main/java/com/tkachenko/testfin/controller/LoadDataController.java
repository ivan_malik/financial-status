package com.tkachenko.testfin.controller;


import com.tkachenko.testfin.dao.DepartmentDao;
import com.tkachenko.testfin.dao.IndicatorsDao;
import com.tkachenko.testfin.model.DepartmentEntity;
import com.tkachenko.testfin.model.IndicatorsEntity;
import com.tkachenko.testfin.util.ExcelParser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Controller
public class LoadDataController {

    @Autowired
    private IndicatorsDao indicatorsDao;
    @Autowired
    private DepartmentDao departmentDao;

    @RequestMapping(value = "uploadExcel", method = RequestMethod.POST)
    public ModelAndView loadDataExcel(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes){
        String UPLOADED_FOLDER = "E://temp//";

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = "";
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            login = userDetail.getUsername();
        }

        ModelAndView modelAndView = new ModelAndView();

        if (file.isEmpty()) {
            modelAndView.addObject("message", "Please select a file to upload");
            modelAndView.setViewName("test");
            return modelAndView;
        }

        try {

            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            IndicatorsEntity indicatorsEntity = ExcelParser.getIndicators(UPLOADED_FOLDER + file.getOriginalFilename());

            modelAndView.addObject("indicators", indicatorsEntity);
            List<IndicatorsEntity> indicators = indicatorsDao.findAllForDepartment(login);
            modelAndView.addObject("allIndicators", indicators);
            modelAndView.setViewName("index");

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e ) {
            e.printStackTrace();
        } catch (NumberFormatException e){
            e.printStackTrace();
        }

        return modelAndView;
    }

    @RequestMapping(value = "fromdb", method = RequestMethod.GET)
    public ModelAndView loadDataFromDB2(HttpServletRequest request){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = "";
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            login = userDetail.getUsername();
        }

        String idIndicator = request.getParameter("indicator");

        IndicatorsEntity indicatorsEntity = indicatorsDao.findById(Integer.parseInt(idIndicator));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("indicators", indicatorsEntity);

        List<IndicatorsEntity> indicators = indicatorsDao.findAllForDepartment(login);
        modelAndView.addObject("allIndicators", indicators);

        return modelAndView;
    }
}
