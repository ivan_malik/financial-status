package com.tkachenko.testfin.controller;

import com.tkachenko.testfin.dao.*;
import com.tkachenko.testfin.fuzzy.ClassicMethod;
import com.tkachenko.testfin.fuzzy.FinancialCoefficients;
import com.tkachenko.testfin.fuzzy.FuzzyMethod;
import com.tkachenko.testfin.model.*;
import com.tkachenko.testfin.util.ConvertToList;
import com.tkachenko.testfin.util.CreateReport;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private IndicatorsDao indicatorsDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private RecvalBagDao recvalBagDao;
    @Autowired
    private RecvalQualgDao recvalQualgDao;
    @Autowired
    private RecvalProfgDao recvalProfgDao;
    @Autowired
    private RecvalLiqgDao recvalLiqgDao;
    @Autowired
    private RecvalFsgDao recvalFsgDao;
    @Autowired
    private BusinessActivityGroupDao businessActivityGroupDao;
    @Autowired
    private FinancialStabilityGroupDao financialStabilityGroupDao;
    @Autowired
    private LiquidityGroupDao liquidityGroupDao;
    @Autowired
    private ProfitabilityGroupDao profitabilityGroupDao;
    @Autowired
    private QualitativeGroupDao qualitativeGroupDao;
    @Autowired
    private ResultDao resultDao;

    private static final Logger logger = Logger.getLogger(MainController.class);

    @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    public ModelAndView defaultPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = "";
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            login = userDetail.getUsername();
        }

        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        List<IndicatorsEntity> indicators = indicatorsDao.findAllForDepartment(login);
        model.addObject("allIndicators", indicators);
        model.addObject("indicators", new IndicatorsEntity());

        return model;

    }

    @RequestMapping(value = { "/history" }, method = RequestMethod.GET)
    public ModelAndView history() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = "";
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            login = userDetail.getUsername();
        }

        ModelAndView model = new ModelAndView();
        List<IndicatorsEntity> indicatorsEntities = indicatorsDao.findAllForDepartment(login);
        ArrayList<History> history = new ArrayList<>();

        for (IndicatorsEntity i: indicatorsEntities) {
            BusinessActivityGroupEntity businessActivityGroupEntity = businessActivityGroupDao.findAllByIndicators(i.getIdindicatorsId());
            LiquidityGroupEntity liquidityGroupEntity = liquidityGroupDao.findAllByIndicators(i.getIdindicatorsId());
            ProfitabilityGroupEntity profitabilityGroupEntity = profitabilityGroupDao.findAllByIndicators(i.getIdindicatorsId());
            FinancialStabilityGroupEntity financialStabilityGroupEntity = financialStabilityGroupDao.findAllByIndicators(i.getIdindicatorsId());
            QualitativeGroupEntity qualitativeGroupEntity = qualitativeGroupDao.findAllByIndicators(i.getIdindicatorsId());
            ResultsEntity resultsEntity = resultDao.findAllByIndicators(i.getIdindicatorsId());

            history.add(new History(i.getCalculationDate(), i.getCalculationTime(), liquidityGroupEntity.getY1(),liquidityGroupEntity.getY2(),
                    liquidityGroupEntity.getY3(), financialStabilityGroupEntity.getY4(),financialStabilityGroupEntity.getY5(),financialStabilityGroupEntity.getY6(),
                    financialStabilityGroupEntity.getY7(),financialStabilityGroupEntity.getY8(), profitabilityGroupEntity.getY9(),profitabilityGroupEntity.getY10(),
                    profitabilityGroupEntity.getY11(),profitabilityGroupEntity.getY12(),businessActivityGroupEntity.getY13(),businessActivityGroupEntity.getY14(),
                    businessActivityGroupEntity.getY15(),businessActivityGroupEntity.getY16(),qualitativeGroupEntity.getY17(),qualitativeGroupEntity.getY18(),
                    liquidityGroupEntity.getZ1(),financialStabilityGroupEntity.getZ2(),profitabilityGroupEntity.getZ3(),businessActivityGroupEntity.getZ4(),
                    qualitativeGroupEntity.getZ5(),resultsEntity.getClassicMethodState(),resultsEntity.getFuzzyMethodState()));
        }

        model.addObject("history", history);
        model.setViewName("history");

        return model;
    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Login Form - Database Authentication");
        model.addObject("message", "This page is for ROLE_ADMIN only!");
        model.setViewName("admin");
        return model;

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;

    }

    //for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());
        }

        model.setViewName("403");
        return model;

    }

    @RequestMapping(value = "/perform", method = RequestMethod.GET)
    public ModelAndView perform(@ModelAttribute IndicatorsEntity indicatorsEntity) {
        ModelAndView modelAndView = new ModelAndView();
        NumberFormat formatter = new DecimalFormat("#0.000");
        ArrayList<Double> params = new ArrayList<>();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = "";
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            login = userDetail.getUsername();
        }

        DepartmentEntity departmentEntity = departmentDao.findByLogin(login);
        indicatorsEntity.setDepartment(departmentEntity);//установить филиал которій считает

        java.util.Date uDate = new java.util.Date();
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        java.sql.Time sTime = new java.sql.Time(uDate.getTime());


        indicatorsEntity.setCalculationDate(sDate);//установить текущую дату
        indicatorsEntity.setCalculationTime(sTime);

        indicatorsDao.save(indicatorsEntity);

        ArrayList<Double> indicatorsList = ConvertToList.getListFromIndicators(indicatorsEntity);
        FinancialCoefficients financialCoefficients = new FinancialCoefficients(indicatorsList);
        FuzzyMethod fuzzyMethod = new FuzzyMethod(financialCoefficients);

        ClassicMethod classicMethod = new ClassicMethod(financialCoefficients);
        String stringResultClassic = classicMethod.getStringResult();
        double doubleResultClassic = classicMethod.getDoubleResult();
        params.add(doubleResultClassic);//0
        modelAndView.addObject("doubleClassic", formatter.format(doubleResultClassic));
        modelAndView.addObject("stringClassic", stringResultClassic);
        try {
            String stringResultFuzzy = fuzzyMethod.getStringResult();
            double doubleResultFuzzy = fuzzyMethod.getDoubleResult();
            params.add(doubleResultFuzzy);//1
            modelAndView.addObject("doubleFuzzy", formatter.format(doubleResultFuzzy));
            modelAndView.addObject("stringFuzzy", stringResultFuzzy);

            double z1 = FuzzyMethod.findZ1(financialCoefficients.getY1(),financialCoefficients.getY2(),financialCoefficients.getY3());
            params.add(z1);//2
            LiquidityGroupEntity liquidityGroupEntity = new LiquidityGroupEntity();
            liquidityGroupEntity.setIndicatorsEntity(indicatorsEntity);
            liquidityGroupEntity.setRecvalLiqgEntity(recvalLiqgDao.findWithMaxDate());
            liquidityGroupEntity.setY1(financialCoefficients.getY1());
            liquidityGroupEntity.setY2(financialCoefficients.getY2());
            liquidityGroupEntity.setY3(financialCoefficients.getY3());
            liquidityGroupEntity.setZ1(z1);
            liquidityGroupDao.save(liquidityGroupEntity); //save

            modelAndView.addObject("z1", formatter.format(z1));

            double z2 = FuzzyMethod.findZ2(financialCoefficients.getY4(),financialCoefficients.getY5(),financialCoefficients.getY6(),
                    financialCoefficients.getY7(),financialCoefficients.getY8());
            params.add(z2);//3
            FinancialStabilityGroupEntity financialStabilityGroupEntity = new FinancialStabilityGroupEntity();
            financialStabilityGroupEntity.setIndicatorsEntity(indicatorsEntity);
            financialStabilityGroupEntity.setRecvalFsgEntity(recvalFsgDao.findWithMaxDate());
            financialStabilityGroupEntity.setY4(financialCoefficients.getY4());
            financialStabilityGroupEntity.setY5(financialCoefficients.getY5());
            financialStabilityGroupEntity.setY6(financialCoefficients.getY6());
            financialStabilityGroupEntity.setY7(financialCoefficients.getY7());
            financialStabilityGroupEntity.setY8(financialCoefficients.getY8());
            financialStabilityGroupEntity.setZ2(z2);
            financialStabilityGroupDao.save(financialStabilityGroupEntity); //save

            modelAndView.addObject("z2", formatter.format(z2));

            double z3 = FuzzyMethod.findZ3(financialCoefficients.getY9(),financialCoefficients.getY10(),financialCoefficients.getY11(),
                    financialCoefficients.getY12());
            params.add(z3);//4
            ProfitabilityGroupEntity profitabilityGroupEntity = new ProfitabilityGroupEntity();
            profitabilityGroupEntity.setIndicatorsEntity(indicatorsEntity);
            profitabilityGroupEntity.setRecvalProfgEntity(recvalProfgDao.findWithMaxDate());
            profitabilityGroupEntity.setY9(financialCoefficients.getY9());
            profitabilityGroupEntity.setY10(financialCoefficients.getY10());
            profitabilityGroupEntity.setY11(financialCoefficients.getY11());
            profitabilityGroupEntity.setY12(financialCoefficients.getY12());
            profitabilityGroupEntity.setZ3(z3);
            profitabilityGroupDao.save(profitabilityGroupEntity); //save

            modelAndView.addObject("z3", formatter.format(z3));

            double z4 = FuzzyMethod.findZ4(financialCoefficients.getY13(),financialCoefficients.getY14(),financialCoefficients.getY15(),
                    financialCoefficients.getY16());
            params.add(z4);//5
            BusinessActivityGroupEntity businessActivityGroupEntity = new BusinessActivityGroupEntity();
            businessActivityGroupEntity.setIndicatorsEntity(indicatorsEntity);
            businessActivityGroupEntity.setRecvalBagEntity(recvalBagDao.findWithMaxDate());
            businessActivityGroupEntity.setZ4(z4);
            businessActivityGroupEntity.setY13(financialCoefficients.getY13());
            businessActivityGroupEntity.setY14(financialCoefficients.getY14());
            businessActivityGroupEntity.setY15(financialCoefficients.getY15());
            businessActivityGroupEntity.setY16(financialCoefficients.getY16());
            businessActivityGroupDao.save(businessActivityGroupEntity); //save

            modelAndView.addObject("z4", formatter.format(z4));

            double y17 = FuzzyMethod.findY17(indicatorsList.get(26), indicatorsList.get(27));
            modelAndView.addObject("y17", formatter.format(y17));
            params.add(y17);//6
            double y18 = FuzzyMethod.findY18(indicatorsList.get(28), indicatorsList.get(29));
            modelAndView.addObject("y18", formatter.format(y18));
            params.add(y18);//7
            double z5 = FuzzyMethod.findZ5(y17, y18);
            params.add(z5);//8
            QualitativeGroupEntity qualitativeGroupEntity = new QualitativeGroupEntity();
            qualitativeGroupEntity.setIndicatorsEntity(indicatorsEntity);
            qualitativeGroupEntity.setRecvalQualgEntity(recvalQualgDao.findWithMaxDate());
            qualitativeGroupEntity.setY17(y17);
            qualitativeGroupEntity.setY18(y18);
            qualitativeGroupEntity.setZ5(z5);
            qualitativeGroupDao.save(qualitativeGroupEntity);//save

            modelAndView.addObject("z5", formatter.format(z5));

            ResultsEntity resultsEntity = new ResultsEntity();
            resultsEntity.setClassicMethodMark(doubleResultClassic);
            resultsEntity.setClassicMethodState(stringResultClassic);
            resultsEntity.setFuzzyMethodDefuzz(doubleResultFuzzy);
            resultsEntity.setFuzzyMethodState(stringResultFuzzy);
            resultsEntity.setIndicatorsEntity(indicatorsEntity);
            resultDao.save(resultsEntity);

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            File fileIN = ResourceUtils.getFile("classpath:exampleReport.xlsx");
            File fileOUT = ResourceUtils.getFile("classpath:createdReport.xlsx");
            CreateReport.modifyExistingWorkbook(financialCoefficients, params, fileIN.getPath(), fileOUT.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        modelAndView.addObject("coefficients", financialCoefficients);
        modelAndView.setViewName("results");
        return modelAndView;
    }

}
