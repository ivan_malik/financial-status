package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "profitability_group", schema = "mydb")
public class ProfitabilityGroupEntity {
    private int profitabilityGroupId;
    private Double y9;
    private Double y10;
    private Double y11;
    private Double y12;
    private Double z3;
    private RecvalProfgEntity recvalProfgEntity;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "recommend_values", nullable = false)
    public RecvalProfgEntity getRecvalProfgEntity() {
        return recvalProfgEntity;
    }

    public void setRecvalProfgEntity(RecvalProfgEntity recvalProfgEntity) {
        this.recvalProfgEntity = recvalProfgEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "profitability_group_id")
    public int getProfitabilityGroupId() {
        return profitabilityGroupId;
    }

    public void setProfitabilityGroupId(int profitabilityGroupId) {
        this.profitabilityGroupId = profitabilityGroupId;
    }

    @Basic
    @Column(name = "y9")
    public Double getY9() {
        return y9;
    }

    public void setY9(Double y9) {
        this.y9 = y9;
    }

    @Basic
    @Column(name = "y10")
    public Double getY10() {
        return y10;
    }

    public void setY10(Double y10) {
        this.y10 = y10;
    }

    @Basic
    @Column(name = "y11")
    public Double getY11() {
        return y11;
    }

    public void setY11(Double y11) {
        this.y11 = y11;
    }

    @Basic
    @Column(name = "y12")
    public Double getY12() {
        return y12;
    }

    public void setY12(Double y12) {
        this.y12 = y12;
    }

    @Basic
    @Column(name = "z3")
    public Double getZ3() {
        return z3;
    }

    public void setZ3(Double z3) {
        this.z3 = z3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfitabilityGroupEntity that = (ProfitabilityGroupEntity) o;

        if (profitabilityGroupId != that.profitabilityGroupId) return false;
        if (y9 != null ? !y9.equals(that.y9) : that.y9 != null) return false;
        if (y10 != null ? !y10.equals(that.y10) : that.y10 != null) return false;
        if (y11 != null ? !y11.equals(that.y11) : that.y11 != null) return false;
        if (y12 != null ? !y12.equals(that.y12) : that.y12 != null) return false;
        if (z3 != null ? !z3.equals(that.z3) : that.z3 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = profitabilityGroupId;
        result = 31 * result + (y9 != null ? y9.hashCode() : 0);
        result = 31 * result + (y10 != null ? y10.hashCode() : 0);
        result = 31 * result + (y11 != null ? y11.hashCode() : 0);
        result = 31 * result + (y12 != null ? y12.hashCode() : 0);
        result = 31 * result + (z3 != null ? z3.hashCode() : 0);
        return result;
    }
}
