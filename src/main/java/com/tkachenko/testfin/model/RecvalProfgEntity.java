package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recval_profg", schema = "mydb")
public class RecvalProfgEntity {
    private int recvalProfgId;
    private Double y9;
    private Double y10;
    private Double y11;
    private Double y12;
    private Date updateDate;
    Set<ProfitabilityGroupEntity> profitabilityGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "recvalProfgEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<ProfitabilityGroupEntity> getProfitabilityGroupEntities() {
        return profitabilityGroupEntities;
    }

    public void setProfitabilityGroupEntities(Set<ProfitabilityGroupEntity> profitabilityGroupEntities) {
        this.profitabilityGroupEntities = profitabilityGroupEntities;
    }

    public RecvalProfgEntity(int recvalProfgId, Double y9, Double y10, Double y11, Double y12, Date updateDate) {
        this.recvalProfgId = recvalProfgId;
        this.y9 = y9;
        this.y10 = y10;
        this.y11 = y11;
        this.y12 = y12;
        this.updateDate = updateDate;
    }

    public RecvalProfgEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recval_profg_id")
    public int getRecvalProfgId() {
        return recvalProfgId;
    }

    public void setRecvalProfgId(int recvalProfgId) {
        this.recvalProfgId = recvalProfgId;
    }

    @Basic
    @Column(name = "y9")
    public Double getY9() {
        return y9;
    }

    public void setY9(Double y9) {
        this.y9 = y9;
    }

    @Basic
    @Column(name = "y10")
    public Double getY10() {
        return y10;
    }

    public void setY10(Double y10) {
        this.y10 = y10;
    }

    @Basic
    @Column(name = "y11")
    public Double getY11() {
        return y11;
    }

    public void setY11(Double y11) {
        this.y11 = y11;
    }

    @Basic
    @Column(name = "y12")
    public Double getY12() {
        return y12;
    }

    public void setY12(Double y12) {
        this.y12 = y12;
    }

    @Basic
    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecvalProfgEntity that = (RecvalProfgEntity) o;

        if (recvalProfgId != that.recvalProfgId) return false;
        if (y9 != null ? !y9.equals(that.y9) : that.y9 != null) return false;
        if (y10 != null ? !y10.equals(that.y10) : that.y10 != null) return false;
        if (y11 != null ? !y11.equals(that.y11) : that.y11 != null) return false;
        if (y12 != null ? !y12.equals(that.y12) : that.y12 != null) return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recvalProfgId;
        result = 31 * result + (y9 != null ? y9.hashCode() : 0);
        result = 31 * result + (y10 != null ? y10.hashCode() : 0);
        result = 31 * result + (y11 != null ? y11.hashCode() : 0);
        result = 31 * result + (y12 != null ? y12.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }
}
