package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "qualitative_group", schema = "mydb")
public class QualitativeGroupEntity {
    private int qualitativeGroupId;
    private Double y17;
    private Double y18;
    private Double z5;
    private RecvalQualgEntity recvalQualgEntity;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "recommend_values", nullable = false)
    public RecvalQualgEntity getRecvalQualgEntity() {
        return recvalQualgEntity;
    }

    public void setRecvalQualgEntity(RecvalQualgEntity recvalQualgEntity) {
        this.recvalQualgEntity = recvalQualgEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qualitative_group_id")
    public int getQualitativeGroupId() {
        return qualitativeGroupId;
    }

    public void setQualitativeGroupId(int qualitativeGroupId) {
        this.qualitativeGroupId = qualitativeGroupId;
    }

    @Basic
    @Column(name = "y17")
    public Double getY17() {
        return y17;
    }

    public void setY17(Double y17) {
        this.y17 = y17;
    }

    @Basic
    @Column(name = "y18")
    public Double getY18() {
        return y18;
    }

    public void setY18(Double y18) {
        this.y18 = y18;
    }

    @Basic
    @Column(name = "z5")
    public Double getZ5() {
        return z5;
    }

    public void setZ5(Double z5) {
        this.z5 = z5;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QualitativeGroupEntity that = (QualitativeGroupEntity) o;

        if (qualitativeGroupId != that.qualitativeGroupId) return false;
        if (y17 != null ? !y17.equals(that.y17) : that.y17 != null) return false;
        if (y18 != null ? !y18.equals(that.y18) : that.y18 != null) return false;
        if (z5 != null ? !z5.equals(that.z5) : that.z5 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = qualitativeGroupId;
        result = 31 * result + (y17 != null ? y17.hashCode() : 0);
        result = 31 * result + (y18 != null ? y18.hashCode() : 0);
        result = 31 * result + (z5 != null ? z5.hashCode() : 0);
        return result;
    }
}
