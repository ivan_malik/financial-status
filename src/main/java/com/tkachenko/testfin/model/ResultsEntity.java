package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "results", schema = "mydb")
public class ResultsEntity {
    private int resultsId;
    private String fuzzyMethodState;
    private Double fuzzyMethodDefuzz;
    private Double classicMethodMark;
    private String classicMethodState;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "results_id")
    public int getResultsId() {
        return resultsId;
    }

    public void setResultsId(int resultsId) {
        this.resultsId = resultsId;
    }

    @Basic
    @Column(name = "fuzzy_method_state")
    public String getFuzzyMethodState() {
        return fuzzyMethodState;
    }

    public void setFuzzyMethodState(String fuzzyMethodState) {
        this.fuzzyMethodState = fuzzyMethodState;
    }

    @Basic
    @Column(name = "fuzzy_method_defuzz")
    public Double getFuzzyMethodDefuzz() {
        return fuzzyMethodDefuzz;
    }

    public void setFuzzyMethodDefuzz(Double fuzzyMethodDefuzz) {
        this.fuzzyMethodDefuzz = fuzzyMethodDefuzz;
    }

    @Basic
    @Column(name = "classic_method_mark")
    public Double getClassicMethodMark() {
        return classicMethodMark;
    }

    public void setClassicMethodMark(Double classicMethodMark) {
        this.classicMethodMark = classicMethodMark;
    }

    @Basic
    @Column(name = "classic_method_state")
    public String getClassicMethodState() {
        return classicMethodState;
    }

    public void setClassicMethodState(String classicMethodState) {
        this.classicMethodState = classicMethodState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultsEntity that = (ResultsEntity) o;

        if (resultsId != that.resultsId) return false;
        if (fuzzyMethodState != null ? !fuzzyMethodState.equals(that.fuzzyMethodState) : that.fuzzyMethodState != null)
            return false;
        if (fuzzyMethodDefuzz != null ? !fuzzyMethodDefuzz.equals(that.fuzzyMethodDefuzz) : that.fuzzyMethodDefuzz != null)
            return false;
        if (classicMethodMark != null ? !classicMethodMark.equals(that.classicMethodMark) : that.classicMethodMark != null)
            return false;
        if (classicMethodState != null ? !classicMethodState.equals(that.classicMethodState) : that.classicMethodState != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = resultsId;
        result = 31 * result + (fuzzyMethodState != null ? fuzzyMethodState.hashCode() : 0);
        result = 31 * result + (fuzzyMethodDefuzz != null ? fuzzyMethodDefuzz.hashCode() : 0);
        result = 31 * result + (classicMethodMark != null ? classicMethodMark.hashCode() : 0);
        result = 31 * result + (classicMethodState != null ? classicMethodState.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ResultsEntity{" +
                "resultsId=" + resultsId +
                ", fuzzyMethodState='" + fuzzyMethodState + '\'' +
                ", fuzzyMethodDefuzz=" + fuzzyMethodDefuzz +
                ", classicMethodMark=" + classicMethodMark +
                ", classicMethodState='" + classicMethodState + '\'' +
                ", indicatorsEntity=" + indicatorsEntity +
                '}';
    }
}
