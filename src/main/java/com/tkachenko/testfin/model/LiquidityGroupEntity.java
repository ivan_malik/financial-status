package com.tkachenko.testfin.model;

import javax.persistence.*;


@Entity
@Table(name = "liquidity_group", schema = "mydb")
public class LiquidityGroupEntity {
    private int liquidityGroupId;
    private Double y1;
    private Double y2;
    private Double y3;
    private Double z1;
    private RecvalLiqgEntity recvalLiqgEntity;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "recommend_values", nullable = false)
    public RecvalLiqgEntity getRecvalLiqgEntity() {
        return recvalLiqgEntity;
    }

    public void setRecvalLiqgEntity(RecvalLiqgEntity recvalLiqgEntity) {
        this.recvalLiqgEntity = recvalLiqgEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "liquidity_group_id")
    public int getLiquidityGroupId() {
        return liquidityGroupId;
    }

    public void setLiquidityGroupId(int liquidityGroupId) {
        this.liquidityGroupId = liquidityGroupId;
    }

    @Basic
    @Column(name = "y1")
    public Double getY1() {
        return y1;
    }

    public void setY1(Double y1) {
        this.y1 = y1;
    }

    @Basic
    @Column(name = "y2")
    public Double getY2() {
        return y2;
    }

    public void setY2(Double y2) {
        this.y2 = y2;
    }

    @Basic
    @Column(name = "y3")
    public Double getY3() {
        return y3;
    }

    public void setY3(Double y3) {
        this.y3 = y3;
    }

    @Basic
    @Column(name = "z1")
    public Double getZ1() {
        return z1;
    }

    public void setZ1(Double z1) {
        this.z1 = z1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LiquidityGroupEntity that = (LiquidityGroupEntity) o;

        if (liquidityGroupId != that.liquidityGroupId) return false;
        if (y1 != null ? !y1.equals(that.y1) : that.y1 != null) return false;
        if (y2 != null ? !y2.equals(that.y2) : that.y2 != null) return false;
        if (y3 != null ? !y3.equals(that.y3) : that.y3 != null) return false;
        if (z1 != null ? !z1.equals(that.z1) : that.z1 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = liquidityGroupId;
        result = 31 * result + (y1 != null ? y1.hashCode() : 0);
        result = 31 * result + (y2 != null ? y2.hashCode() : 0);
        result = 31 * result + (y3 != null ? y3.hashCode() : 0);
        result = 31 * result + (z1 != null ? z1.hashCode() : 0);
        return result;
    }
}
