package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "country", schema = "mydb")
public class CountryEntity {
    private int countryId;
    private String name;
    private Set<CityEntity> cityEntities = new HashSet<>();

    public CountryEntity(int countryId, String name) {
        this.countryId = countryId;
        this.name = name;
    }

    public CountryEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "country", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<CityEntity> getCityEntities() {
        return cityEntities;
    }

    public void setCityEntities(Set<CityEntity> cityEntities) {
        this.cityEntities = cityEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryEntity that = (CountryEntity) o;

        if (countryId != that.countryId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = countryId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CountryEntity{" +
                "countryId=" + countryId +
                ", name='" + name + '\'' +
                '}';
    }
}
