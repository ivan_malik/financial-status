package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recval_fsg", schema = "mydb")
public class RecvalFsgEntity {
    private int recvalFsgId;
    private Double y4;
    private Double y5;
    private Double y6;
    private Double y7;
    private Double y8;
    private Date updateDate;
    private Set<FinancialStabilityGroupEntity> financialStabilityGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "recvalFsgEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<FinancialStabilityGroupEntity> getFinancialStabilityGroupEntities() {
        return financialStabilityGroupEntities;
    }

    public void setFinancialStabilityGroupEntities(Set<FinancialStabilityGroupEntity> financialStabilityGroupEntities) {
        this.financialStabilityGroupEntities = financialStabilityGroupEntities;
    }

    public RecvalFsgEntity(int recvalFsgId, Double y4, Double y5, Double y6, Double y7, Double y8, Date updateDate) {
        this.recvalFsgId = recvalFsgId;
        this.y4 = y4;
        this.y5 = y5;
        this.y6 = y6;
        this.y7 = y7;
        this.y8 = y8;
        this.updateDate = updateDate;
    }

    public RecvalFsgEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recval_fsg_id")
    public int getRecvalFsgId() {
        return recvalFsgId;
    }

    public void setRecvalFsgId(int recvalFsgId) {
        this.recvalFsgId = recvalFsgId;
    }

    @Basic
    @Column(name = "y4")
    public Double getY4() {
        return y4;
    }

    public void setY4(Double y4) {
        this.y4 = y4;
    }

    @Basic
    @Column(name = "y5")
    public Double getY5() {
        return y5;
    }

    public void setY5(Double y5) {
        this.y5 = y5;
    }

    @Basic
    @Column(name = "y6")
    public Double getY6() {
        return y6;
    }

    public void setY6(Double y6) {
        this.y6 = y6;
    }

    @Basic
    @Column(name = "y7")
    public Double getY7() {
        return y7;
    }

    public void setY7(Double y7) {
        this.y7 = y7;
    }

    @Basic
    @Column(name = "y8")
    public Double getY8() {
        return y8;
    }

    public void setY8(Double y8) {
        this.y8 = y8;
    }

    @Basic
    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecvalFsgEntity that = (RecvalFsgEntity) o;

        if (recvalFsgId != that.recvalFsgId) return false;
        if (y4 != null ? !y4.equals(that.y4) : that.y4 != null) return false;
        if (y5 != null ? !y5.equals(that.y5) : that.y5 != null) return false;
        if (y6 != null ? !y6.equals(that.y6) : that.y6 != null) return false;
        if (y7 != null ? !y7.equals(that.y7) : that.y7 != null) return false;
        if (y8 != null ? !y8.equals(that.y8) : that.y8 != null) return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recvalFsgId;
        result = 31 * result + (y4 != null ? y4.hashCode() : 0);
        result = 31 * result + (y5 != null ? y5.hashCode() : 0);
        result = 31 * result + (y6 != null ? y6.hashCode() : 0);
        result = 31 * result + (y7 != null ? y7.hashCode() : 0);
        result = 31 * result + (y8 != null ? y8.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }
}
