package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recval_bag", schema = "mydb")
public class RecvalBagEntity {
    private int recvalBagId;
    private Double y13;
    private Double y14;
    private Double y15;
    private Double y16;
    private Date updateDate;
    private Set<BusinessActivityGroupEntity> businessActivityGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "recvalBagEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<BusinessActivityGroupEntity> getBusinessActivityGroupEntities() {
        return businessActivityGroupEntities;
    }

    public void setBusinessActivityGroupEntities(Set<BusinessActivityGroupEntity> businessActivityGroupEntities) {
        this.businessActivityGroupEntities = businessActivityGroupEntities;
    }

    public RecvalBagEntity(int recvalBagId, Double y13, Double y14, Double y15, Double y16, Date updateDate) {
        this.recvalBagId = recvalBagId;
        this.y13 = y13;
        this.y14 = y14;
        this.y15 = y15;
        this.y16 = y16;
        this.updateDate = updateDate;
    }

    public RecvalBagEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recval_bag_id")
    public int getRecvalBagId() {
        return recvalBagId;
    }

    public void setRecvalBagId(int recvalBagId) {
        this.recvalBagId = recvalBagId;
    }

    @Basic
    @Column(name = "y13")
    public Double getY13() {
        return y13;
    }

    public void setY13(Double y13) {
        this.y13 = y13;
    }

    @Basic
    @Column(name = "y14")
    public Double getY14() {
        return y14;
    }

    public void setY14(Double y14) {
        this.y14 = y14;
    }

    @Basic
    @Column(name = "y15")
    public Double getY15() {
        return y15;
    }

    public void setY15(Double y15) {
        this.y15 = y15;
    }

    @Basic
    @Column(name = "y16")
    public Double getY16() {
        return y16;
    }

    public void setY16(Double y16) {
        this.y16 = y16;
    }

    @Basic
    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecvalBagEntity that = (RecvalBagEntity) o;

        if (recvalBagId != that.recvalBagId) return false;
        if (y13 != null ? !y13.equals(that.y13) : that.y13 != null) return false;
        if (y14 != null ? !y14.equals(that.y14) : that.y14 != null) return false;
        if (y15 != null ? !y15.equals(that.y15) : that.y15 != null) return false;
        if (y16 != null ? !y16.equals(that.y16) : that.y16 != null) return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recvalBagId;
        result = 31 * result + (y13 != null ? y13.hashCode() : 0);
        result = 31 * result + (y14 != null ? y14.hashCode() : 0);
        result = 31 * result + (y15 != null ? y15.hashCode() : 0);
        result = 31 * result + (y16 != null ? y16.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }
}
