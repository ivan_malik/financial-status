package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recval_liqg", schema = "mydb")
public class RecvalLiqgEntity {
    private int idRecvalLg;
    private Double y1;
    private Double y2;
    private Double y3;
    private Date updateDate;
    private Set<LiquidityGroupEntity> liquidityGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "recvalLiqgEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<LiquidityGroupEntity> getLiquidityGroupEntities() {
        return liquidityGroupEntities;
    }

    public void setLiquidityGroupEntities(Set<LiquidityGroupEntity> liquidityGroupEntities) {
        this.liquidityGroupEntities = liquidityGroupEntities;
    }

    public RecvalLiqgEntity(int idRecvalLg, Double y1, Double y2, Double y3, Date updateDate) {
        this.idRecvalLg = idRecvalLg;
        this.y1 = y1;
        this.y2 = y2;
        this.y3 = y3;
        this.updateDate = updateDate;
    }

    public RecvalLiqgEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_recval_lg")
    public int getIdRecvalLg() {
        return idRecvalLg;
    }

    public void setIdRecvalLg(int idRecvalLg) {
        this.idRecvalLg = idRecvalLg;
    }

    @Basic
    @Column(name = "y1")
    public Double getY1() {
        return y1;
    }

    public void setY1(Double y1) {
        this.y1 = y1;
    }

    @Basic
    @Column(name = "y2")
    public Double getY2() {
        return y2;
    }

    public void setY2(Double y2) {
        this.y2 = y2;
    }

    @Basic
    @Column(name = "y3")
    public Double getY3() {
        return y3;
    }

    public void setY3(Double y3) {
        this.y3 = y3;
    }

    @Basic
    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecvalLiqgEntity that = (RecvalLiqgEntity) o;

        if (idRecvalLg != that.idRecvalLg) return false;
        if (y1 != null ? !y1.equals(that.y1) : that.y1 != null) return false;
        if (y2 != null ? !y2.equals(that.y2) : that.y2 != null) return false;
        if (y3 != null ? !y3.equals(that.y3) : that.y3 != null) return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRecvalLg;
        result = 31 * result + (y1 != null ? y1.hashCode() : 0);
        result = 31 * result + (y2 != null ? y2.hashCode() : 0);
        result = 31 * result + (y3 != null ? y3.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }
}
