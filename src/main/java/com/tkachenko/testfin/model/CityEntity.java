package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "city", schema = "mydb")
public class CityEntity {
    private int cityId;
    private String title;
    private CountryEntity country;
    private Set<DepartmentEntity> departmentEntities = new HashSet<>();

    @OneToMany(mappedBy = "cityEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<DepartmentEntity> getDepartmentEntities() {
        return departmentEntities;
    }

    public void setDepartmentEntities(Set<DepartmentEntity> departmentEntities) {
        this.departmentEntities = departmentEntities;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityEntity that = (CityEntity) o;

        if (cityId != that.cityId) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cityId;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CityEntity{" +
                "cityId=" + cityId +
                ", title='" + title + '\'' +
                ", country=" + country +
                '}';
    }
}
