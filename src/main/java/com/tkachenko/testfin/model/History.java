package com.tkachenko.testfin.model;


import java.sql.Date;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class History {

    private NumberFormat formatter = new DecimalFormat("#0.000");

    private Date date;
    private int year;
    private int month;
    private int day;
    private Time time;
    private double y1;
    private double y2;
    private double y3;
    private double y4;
    private double y5;
    private double y6;
    private double y7;
    private double y8;
    private double y9;
    private double y10;
    private double y11;
    private double y12;
    private double y13;
    private double y14;
    private double y15;
    private double y16;
    private double y17;
    private double y18;
    private double z1;
    private double z2;
    private double z3;
    private double z4;
    private double z5;
    private String resultCl;
    private String resultFz;

    public History(Date date, Time time, double y1, double y2, double y3, double y4, double y5, double y6, double y7, double y8, double y9, double y10, double y11, double y12, double y13, double y14, double y15, double y16, double y17, double y18, double z1, double z2, double z3, double z4, double z5, String resultCl, String resultFz) {
        this.date = date;
        this.time = time;
        this.y1 = y1;
        this.y2 = y2;
        this.y3 = y3;
        this.y4 = y4;
        this.y5 = y5;
        this.y6 = y6;
        this.y7 = y7;
        this.y8 = y8;
        this.y9 = y9;
        this.y10 = y10;
        this.y11 = y11;
        this.y12 = y12;
        this.y13 = y13;
        this.y14 = y14;
        this.y15 = y15;
        this.y16 = y16;
        this.y17 = y17;
        this.y18 = y18;
        this.z1 = z1;
        this.z2 = z2;
        this.z3 = z3;
        this.z4 = z4;
        this.z5 = z5;
        this.resultCl = resultCl;
        this.resultFz = resultFz;
    }

    public Date getDate() {
        return date;
    }

    public int getYear() {
        return convertYear(date);
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return convertMonth(date);
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return convertDay(date);
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public double getY1() {
        return Double.parseDouble(formatter.format(y1).replace(",","."));
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public double getY2() {
        return Double.parseDouble(formatter.format(y2).replace(",","."));
    }

    public void setY2(double y2) {
        this.y2 = y2;
    }

    public double getY3() {
        return Double.parseDouble(formatter.format(y3).replace(",","."));
    }

    public void setY3(double y3) {
        this.y3 = y3;
    }

    public double getY4() {
        return Double.parseDouble(formatter.format(y4).replace(",","."));
    }

    public void setY4(double y4) {
        this.y4 = y4;
    }

    public double getY5() {
        return Double.parseDouble(formatter.format(y5).replace(",","."));
    }

    public void setY5(double y5) {
        this.y5 = y5;
    }

    public double getY6() {
        return Double.parseDouble(formatter.format(y6).replace(",","."));
    }

    public void setY6(double y6) {
        this.y6 = y6;
    }

    public double getY7() {
        return Double.parseDouble(formatter.format(y7).replace(",","."));
    }

    public void setY7(double y7) {
        this.y7 = y7;
    }

    public double getY8() {
        return Double.parseDouble(formatter.format(y8).replace(",","."));
    }

    public void setY8(double y8) {
        this.y8 = y8;
    }

    public double getY9() {
        return Double.parseDouble(formatter.format(y9).replace(",","."));
    }

    public void setY9(double y9) {
        this.y9 = y9;
    }

    public double getY10() {
        return Double.parseDouble(formatter.format(y10).replace(",","."));
    }

    public void setY10(double y10) {
        this.y10 = y10;
    }

    public double getY11() {
        return Double.parseDouble(formatter.format(y11).replace(",","."));
    }

    public void setY11(double y11) {
        this.y11 = y11;
    }

    public double getY12() {
        return Double.parseDouble(formatter.format(y12).replace(",","."));
    }

    public void setY12(double y12) {
        this.y12 = y12;
    }

    public double getY13() {
        return Double.parseDouble(formatter.format(y13).replace(",","."));
    }

    public void setY13(double y13) {
        this.y13 = y13;
    }

    public double getY14() {
        return Double.parseDouble(formatter.format(y14).replace(",","."));
    }

    public void setY14(double y14) {
        this.y14 = y14;
    }

    public double getY15() {
        return Double.parseDouble(formatter.format(y15).replace(",","."));
    }

    public void setY15(double y15) {
        this.y15 = y15;
    }

    public double getY16() {
        return Double.parseDouble(formatter.format(y16).replace(",","."));
    }

    public void setY16(double y16) {
        this.y16 = y16;
    }

    public double getY17() {
        return Double.parseDouble(formatter.format(y17).replace(",","."));
    }

    public void setY17(double y17) {
        this.y17 = y17;
    }

    public double getY18() {
        return Double.parseDouble(formatter.format(y18).replace(",","."));
    }

    public void setY18(double y18) {
        this.y18 = y18;
    }

    public double getZ1() {
        return Double.parseDouble(formatter.format(z1).replace(",","."));
    }

    public void setZ1(double z1) {
        this.z1 = z1;
    }

    public double getZ2() {
        return Double.parseDouble(formatter.format(z2).replace(",","."));
    }

    public void setZ2(double z2) {
        this.z2 = z2;
    }

    public double getZ3() {
        return Double.parseDouble(formatter.format(z3).replace(",","."));
    }

    public void setZ3(double z3) {
        this.z3 = z3;
    }

    public double getZ4() {
        return Double.parseDouble(formatter.format(z4).replace(",","."));
    }

    public void setZ4(double z4) {
        this.z4 = z4;
    }

    public double getZ5() {
        return Double.parseDouble(formatter.format(z5).replace(",","."));
    }

    public void setZ5(double z5) {
        this.z5 = z5;
    }

    public String getResultCl() {
        return resultCl;
    }

    public void setResultCl(String resultCl) {
        this.resultCl = resultCl;
    }

    public String getResultFz() {
        return resultFz;
    }

    public void setResultFz(String resultFz) {
        this.resultFz = resultFz;
    }

    private static int convertYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    private static int convertMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    private static int convertDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }
}
