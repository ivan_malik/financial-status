package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "business_activity_group", schema = "mydb")
public class BusinessActivityGroupEntity {
    private int businessActivityGroupId;
    private Double y13;
    private Double y14;
    private Double y15;
    private Double y16;
    private Double z4;
    private RecvalBagEntity recvalBagEntity;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "recommend_values", nullable = false)
    public RecvalBagEntity getRecvalBagEntity() {
        return recvalBagEntity;
    }

    public void setRecvalBagEntity(RecvalBagEntity recvalBagEntity) {
        this.recvalBagEntity = recvalBagEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "business_activity_group_id")
    public int getBusinessActivityGroupId() {
        return businessActivityGroupId;
    }

    public void setBusinessActivityGroupId(int businessActivityFroupId) {
        this.businessActivityGroupId = businessActivityFroupId;
    }

    @Basic
    @Column(name = "y13")
    public Double getY13() {
        return y13;
    }

    public void setY13(Double y13) {
        this.y13 = y13;
    }

    @Basic
    @Column(name = "y14")
    public Double getY14() {
        return y14;
    }

    public void setY14(Double y14) {
        this.y14 = y14;
    }

    @Basic
    @Column(name = "y15")
    public Double getY15() {
        return y15;
    }

    public void setY15(Double y15) {
        this.y15 = y15;
    }

    @Basic
    @Column(name = "y16")
    public Double getY16() {
        return y16;
    }

    public void setY16(Double y16) {
        this.y16 = y16;
    }

    @Basic
    @Column(name = "z4")
    public Double getZ4() {
        return z4;
    }

    public void setZ4(Double z4) {
        this.z4 = z4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusinessActivityGroupEntity that = (BusinessActivityGroupEntity) o;

        if (businessActivityGroupId != that.businessActivityGroupId) return false;
        if (y13 != null ? !y13.equals(that.y13) : that.y13 != null) return false;
        if (y14 != null ? !y14.equals(that.y14) : that.y14 != null) return false;
        if (y15 != null ? !y15.equals(that.y15) : that.y15 != null) return false;
        if (y16 != null ? !y16.equals(that.y16) : that.y16 != null) return false;
        if (z4 != null ? !z4.equals(that.z4) : that.z4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = businessActivityGroupId;
        result = 31 * result + (y13 != null ? y13.hashCode() : 0);
        result = 31 * result + (y14 != null ? y14.hashCode() : 0);
        result = 31 * result + (y15 != null ? y15.hashCode() : 0);
        result = 31 * result + (y16 != null ? y16.hashCode() : 0);
        result = 31 * result + (z4 != null ? z4.hashCode() : 0);
        return result;
    }
}
