package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "recval_qualg", schema = "mydb")
public class RecvalQualgEntity {
    private int recvalQualgId;
    private Double y17;
    private Double y18;
    private Date updateDate;
    private Set<QualitativeGroupEntity> qualitativeGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "recvalQualgEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<QualitativeGroupEntity> getQualitativeGroupEntities() {
        return qualitativeGroupEntities;
    }

    public void setQualitativeGroupEntities(Set<QualitativeGroupEntity> qualitativeGroupEntities) {
        this.qualitativeGroupEntities = qualitativeGroupEntities;
    }

    public RecvalQualgEntity(int recvalQualgId, Double y17, Double y18, Date updateDate) {
        this.recvalQualgId = recvalQualgId;
        this.y17 = y17;
        this.y18 = y18;
        this.updateDate = updateDate;
    }

    public RecvalQualgEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recval_qualg_id")
    public int getRecvalQualgId() {
        return recvalQualgId;
    }

    public void setRecvalQualgId(int recvalQualgId) {
        this.recvalQualgId = recvalQualgId;
    }

    @Basic
    @Column(name = "y17")
    public Double getY17() {
        return y17;
    }

    public void setY17(Double y17) {
        this.y17 = y17;
    }

    @Basic
    @Column(name = "y18")
    public Double getY18() {
        return y18;
    }

    public void setY18(Double y18) {
        this.y18 = y18;
    }

    @Basic
    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecvalQualgEntity that = (RecvalQualgEntity) o;

        if (recvalQualgId != that.recvalQualgId) return false;
        if (y17 != null ? !y17.equals(that.y17) : that.y17 != null) return false;
        if (y18 != null ? !y18.equals(that.y18) : that.y18 != null) return false;
        if (updateDate != null ? !updateDate.equals(that.updateDate) : that.updateDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recvalQualgId;
        result = 31 * result + (y17 != null ? y17.hashCode() : 0);
        result = 31 * result + (y18 != null ? y18.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }
}
