package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "indicators", schema = "mydb")
public class IndicatorsEntity {
    private int idindicatorsId;
    private Date calculationDate;
    private Time calculationTime;
    private Double x1;
    private Double x2;
    private Double x3;
    private Double x4;
    private Double x5;
    private Double x6;
    private Double x7;
    private Double x8;
    private Double x9;
    private Double x10;
    private Double x11;
    private Double x12;
    private Double x13;
    private Double x14;
    private Double x15;
    private Double x16;
    private Double x17;
    private Double x18;
    private Double x19;
    private Double x20;
    private Double x21;
    private Double x22;
    private Double x23;
    private Double x24;
    private Double x25;
    private Double x26;
    private Double x27;
    private Double x28;
    private Double x29;
    private Double x30;
    private DepartmentEntity department;
    private Set<ResultsEntity> resultsEntities = new HashSet<>();
    private Set<FinancialStabilityGroupEntity> financialStabilityGroupEntities = new HashSet<>();
    private Set<LiquidityGroupEntity> liquidityGroupEntities = new HashSet<>();
    private Set<ProfitabilityGroupEntity> profitabilityGroupEntities = new HashSet<>();
    private Set<BusinessActivityGroupEntity> businessActivityGroupEntities = new HashSet<>();
    private Set<QualitativeGroupEntity> qualitativeGroupEntities = new HashSet<>();

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<QualitativeGroupEntity> getQualitativeGroupEntities() {
        return qualitativeGroupEntities;
    }

    public void setQualitativeGroupEntities(Set<QualitativeGroupEntity> qualitativeGroupEntities) {
        this.qualitativeGroupEntities = qualitativeGroupEntities;
    }

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<BusinessActivityGroupEntity> getBusinessActivityGroupEntities() {
        return businessActivityGroupEntities;
    }

    public void setBusinessActivityGroupEntities(Set<BusinessActivityGroupEntity> businessActivityGroupEntities) {
        this.businessActivityGroupEntities = businessActivityGroupEntities;
    }

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<ProfitabilityGroupEntity> getProfitabilityGroupEntities() {
        return profitabilityGroupEntities;
    }

    public void setProfitabilityGroupEntities(Set<ProfitabilityGroupEntity> profitabilityGroupEntities) {
        this.profitabilityGroupEntities = profitabilityGroupEntities;
    }

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<LiquidityGroupEntity> getLiquidityGroupEntities() {
        return liquidityGroupEntities;
    }

    public void setLiquidityGroupEntities(Set<LiquidityGroupEntity> liquidityGroupEntities) {
        this.liquidityGroupEntities = liquidityGroupEntities;
    }

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<FinancialStabilityGroupEntity> getFinancialStabilityGroupEntities() {
        return financialStabilityGroupEntities;
    }

    public void setFinancialStabilityGroupEntities(Set<FinancialStabilityGroupEntity> financialStabilityGroupEntities) {
        this.financialStabilityGroupEntities = financialStabilityGroupEntities;
    }

    @OneToMany(mappedBy = "indicatorsEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<ResultsEntity> getResultsEntities() {
        return resultsEntities;
    }

    public void setResultsEntities(Set<ResultsEntity> resultsEntities) {
        this.resultsEntities = resultsEntities;
    }

    @ManyToOne
    @JoinColumn(name = "department", nullable = false)
    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public IndicatorsEntity(int idindicatorsId, Date calculationDate, Double x1, Double x2, Double x3, Double x4, Double x5, Double x6, Double x7, Double x8, Double x9, Double x10, Double x11, Double x12, Double x13, Double x14, Double x15, Double x16, Double x17, Double x18, Double x19, Double x20, Double x21, Double x22, Double x23, Double x24, Double x25, Double x26, Double x27, Double x28, Double x29, Double x30, DepartmentEntity department) {
        this.idindicatorsId = idindicatorsId;
        this.calculationDate = calculationDate;
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.x5 = x5;
        this.x6 = x6;
        this.x7 = x7;
        this.x8 = x8;
        this.x9 = x9;
        this.x10 = x10;
        this.x11 = x11;
        this.x12 = x12;
        this.x13 = x13;
        this.x14 = x14;
        this.x15 = x15;
        this.x16 = x16;
        this.x17 = x17;
        this.x18 = x18;
        this.x19 = x19;
        this.x20 = x20;
        this.x21 = x21;
        this.x22 = x22;
        this.x23 = x23;
        this.x24 = x24;
        this.x25 = x25;
        this.x26 = x26;
        this.x27 = x27;
        this.x28 = x28;
        this.x29 = x29;
        this.x30 = x30;
        this.department = department;
    }

    public IndicatorsEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idindicators_id")
    public int getIdindicatorsId() {
        return idindicatorsId;
    }

    public void setIdindicatorsId(int idindicatorsId) {
        this.idindicatorsId = idindicatorsId;
    }

    @Basic
    @Column(name = "calculation_date")
    public Date getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Date calculationDate) {
        this.calculationDate = calculationDate;
    }

    @Basic
    @Column(name = "calculation_time")
    public Time getCalculationTime() {
        return calculationTime;
    }

    public void setCalculationTime(Time calculationTime) {
        this.calculationTime = calculationTime;
    }

    @Basic
    @Column(name = "x1")
    public Double getX1() {
        return x1;
    }

    public void setX1(Double x1) {
        this.x1 = x1;
    }

    @Basic
    @Column(name = "x2")
    public Double getX2() {
        return x2;
    }

    public void setX2(Double x2) {
        this.x2 = x2;
    }

    @Basic
    @Column(name = "x3")
    public Double getX3() {
        return x3;
    }

    public void setX3(Double x3) {
        this.x3 = x3;
    }

    @Basic
    @Column(name = "x4")
    public Double getX4() {
        return x4;
    }

    public void setX4(Double x4) {
        this.x4 = x4;
    }

    @Basic
    @Column(name = "x5")
    public Double getX5() {
        return x5;
    }

    public void setX5(Double x5) {
        this.x5 = x5;
    }

    @Basic
    @Column(name = "x6")
    public Double getX6() {
        return x6;
    }

    public void setX6(Double x6) {
        this.x6 = x6;
    }

    @Basic
    @Column(name = "x7")
    public Double getX7() {
        return x7;
    }

    public void setX7(Double x7) {
        this.x7 = x7;
    }

    @Basic
    @Column(name = "x8")
    public Double getX8() {
        return x8;
    }

    public void setX8(Double x8) {
        this.x8 = x8;
    }

    @Basic
    @Column(name = "x9")
    public Double getX9() {
        return x9;
    }

    public void setX9(Double x9) {
        this.x9 = x9;
    }

    @Basic
    @Column(name = "x10")
    public Double getX10() {
        return x10;
    }

    public void setX10(Double x10) {
        this.x10 = x10;
    }

    @Basic
    @Column(name = "x11")
    public Double getX11() {
        return x11;
    }

    public void setX11(Double x11) {
        this.x11 = x11;
    }

    @Basic
    @Column(name = "x12")
    public Double getX12() {
        return x12;
    }

    public void setX12(Double x12) {
        this.x12 = x12;
    }

    @Basic
    @Column(name = "x13")
    public Double getX13() {
        return x13;
    }

    public void setX13(Double x13) {
        this.x13 = x13;
    }

    @Basic
    @Column(name = "x14")
    public Double getX14() {
        return x14;
    }

    public void setX14(Double x14) {
        this.x14 = x14;
    }

    @Basic
    @Column(name = "x15")
    public Double getX15() {
        return x15;
    }

    public void setX15(Double x15) {
        this.x15 = x15;
    }

    @Basic
    @Column(name = "x16")
    public Double getX16() {
        return x16;
    }

    public void setX16(Double x16) {
        this.x16 = x16;
    }

    @Basic
    @Column(name = "x17")
    public Double getX17() {
        return x17;
    }

    public void setX17(Double x17) {
        this.x17 = x17;
    }

    @Basic
    @Column(name = "x18")
    public Double getX18() {
        return x18;
    }

    public void setX18(Double x18) {
        this.x18 = x18;
    }

    @Basic
    @Column(name = "x19")
    public Double getX19() {
        return x19;
    }

    public void setX19(Double x19) {
        this.x19 = x19;
    }

    @Basic
    @Column(name = "x20")
    public Double getX20() {
        return x20;
    }

    public void setX20(Double x20) {
        this.x20 = x20;
    }

    @Basic
    @Column(name = "x21")
    public Double getX21() {
        return x21;
    }

    public void setX21(Double x21) {
        this.x21 = x21;
    }

    @Basic
    @Column(name = "x22")
    public Double getX22() {
        return x22;
    }

    public void setX22(Double x22) {
        this.x22 = x22;
    }

    @Basic
    @Column(name = "x23")
    public Double getX23() {
        return x23;
    }

    public void setX23(Double x23) {
        this.x23 = x23;
    }

    @Basic
    @Column(name = "x24")
    public Double getX24() {
        return x24;
    }

    public void setX24(Double x24) {
        this.x24 = x24;
    }

    @Basic
    @Column(name = "x25")
    public Double getX25() {
        return x25;
    }

    public void setX25(Double x25) {
        this.x25 = x25;
    }

    @Basic
    @Column(name = "x26")
    public Double getX26() {
        return x26;
    }

    public void setX26(Double x26) {
        this.x26 = x26;
    }

    @Basic
    @Column(name = "x27")
    public Double getX27() {
        return x27;
    }

    public void setX27(Double x27) {
        this.x27 = x27;
    }

    @Basic
    @Column(name = "x28")
    public Double getX28() {
        return x28;
    }

    public void setX28(Double x28) {
        this.x28 = x28;
    }

    @Basic
    @Column(name = "x29")
    public Double getX29() {
        return x29;
    }

    public void setX29(Double x29) {
        this.x29 = x29;
    }

    @Basic
    @Column(name = "x30")
    public Double getX30() {
        return x30;
    }

    public void setX30(Double x30) {
        this.x30 = x30;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndicatorsEntity that = (IndicatorsEntity) o;

        if (idindicatorsId != that.idindicatorsId) return false;
        if (calculationDate != null ? !calculationDate.equals(that.calculationDate) : that.calculationDate != null)
            return false;
        if (x1 != null ? !x1.equals(that.x1) : that.x1 != null) return false;
        if (x2 != null ? !x2.equals(that.x2) : that.x2 != null) return false;
        if (x3 != null ? !x3.equals(that.x3) : that.x3 != null) return false;
        if (x4 != null ? !x4.equals(that.x4) : that.x4 != null) return false;
        if (x5 != null ? !x5.equals(that.x5) : that.x5 != null) return false;
        if (x6 != null ? !x6.equals(that.x6) : that.x6 != null) return false;
        if (x7 != null ? !x7.equals(that.x7) : that.x7 != null) return false;
        if (x8 != null ? !x8.equals(that.x8) : that.x8 != null) return false;
        if (x9 != null ? !x9.equals(that.x9) : that.x9 != null) return false;
        if (x10 != null ? !x10.equals(that.x10) : that.x10 != null) return false;
        if (x11 != null ? !x11.equals(that.x11) : that.x11 != null) return false;
        if (x12 != null ? !x12.equals(that.x12) : that.x12 != null) return false;
        if (x13 != null ? !x13.equals(that.x13) : that.x13 != null) return false;
        if (x14 != null ? !x14.equals(that.x14) : that.x14 != null) return false;
        if (x15 != null ? !x15.equals(that.x15) : that.x15 != null) return false;
        if (x16 != null ? !x16.equals(that.x16) : that.x16 != null) return false;
        if (x17 != null ? !x17.equals(that.x17) : that.x17 != null) return false;
        if (x18 != null ? !x18.equals(that.x18) : that.x18 != null) return false;
        if (x19 != null ? !x19.equals(that.x19) : that.x19 != null) return false;
        if (x20 != null ? !x20.equals(that.x20) : that.x20 != null) return false;
        if (x21 != null ? !x21.equals(that.x21) : that.x21 != null) return false;
        if (x22 != null ? !x22.equals(that.x22) : that.x22 != null) return false;
        if (x23 != null ? !x23.equals(that.x23) : that.x23 != null) return false;
        if (x24 != null ? !x24.equals(that.x24) : that.x24 != null) return false;
        if (x25 != null ? !x25.equals(that.x25) : that.x25 != null) return false;
        if (x26 != null ? !x26.equals(that.x26) : that.x26 != null) return false;
        if (x27 != null ? !x27.equals(that.x27) : that.x27 != null) return false;
        if (x28 != null ? !x28.equals(that.x28) : that.x28 != null) return false;
        if (x29 != null ? !x29.equals(that.x29) : that.x29 != null) return false;
        if (x30 != null ? !x30.equals(that.x30) : that.x30 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idindicatorsId;
        result = 31 * result + (calculationDate != null ? calculationDate.hashCode() : 0);
        result = 31 * result + (x1 != null ? x1.hashCode() : 0);
        result = 31 * result + (x2 != null ? x2.hashCode() : 0);
        result = 31 * result + (x3 != null ? x3.hashCode() : 0);
        result = 31 * result + (x4 != null ? x4.hashCode() : 0);
        result = 31 * result + (x5 != null ? x5.hashCode() : 0);
        result = 31 * result + (x6 != null ? x6.hashCode() : 0);
        result = 31 * result + (x7 != null ? x7.hashCode() : 0);
        result = 31 * result + (x8 != null ? x8.hashCode() : 0);
        result = 31 * result + (x9 != null ? x9.hashCode() : 0);
        result = 31 * result + (x10 != null ? x10.hashCode() : 0);
        result = 31 * result + (x11 != null ? x11.hashCode() : 0);
        result = 31 * result + (x12 != null ? x12.hashCode() : 0);
        result = 31 * result + (x13 != null ? x13.hashCode() : 0);
        result = 31 * result + (x14 != null ? x14.hashCode() : 0);
        result = 31 * result + (x15 != null ? x15.hashCode() : 0);
        result = 31 * result + (x16 != null ? x16.hashCode() : 0);
        result = 31 * result + (x17 != null ? x17.hashCode() : 0);
        result = 31 * result + (x18 != null ? x18.hashCode() : 0);
        result = 31 * result + (x19 != null ? x19.hashCode() : 0);
        result = 31 * result + (x20 != null ? x20.hashCode() : 0);
        result = 31 * result + (x21 != null ? x21.hashCode() : 0);
        result = 31 * result + (x22 != null ? x22.hashCode() : 0);
        result = 31 * result + (x23 != null ? x23.hashCode() : 0);
        result = 31 * result + (x24 != null ? x24.hashCode() : 0);
        result = 31 * result + (x25 != null ? x25.hashCode() : 0);
        result = 31 * result + (x26 != null ? x26.hashCode() : 0);
        result = 31 * result + (x27 != null ? x27.hashCode() : 0);
        result = 31 * result + (x28 != null ? x28.hashCode() : 0);
        result = 31 * result + (x29 != null ? x29.hashCode() : 0);
        result = 31 * result + (x30 != null ? x30.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IndicatorsEntity{" +
                "idindicatorsId=" + idindicatorsId +
                ", calculationDate=" + calculationDate +
                ", x1=" + x1 +
                ", x2=" + x2 +
                ", x3=" + x3 +
                ", x4=" + x4 +
                ", x5=" + x5 +
                ", x6=" + x6 +
                ", x7=" + x7 +
                ", x8=" + x8 +
                ", x9=" + x9 +
                ", x10=" + x10 +
                ", x11=" + x11 +
                ", x12=" + x12 +
                ", x13=" + x13 +
                ", x14=" + x14 +
                ", x15=" + x15 +
                ", x16=" + x16 +
                ", x17=" + x17 +
                ", x18=" + x18 +
                ", x19=" + x19 +
                ", x20=" + x20 +
                ", x21=" + x21 +
                ", x22=" + x22 +
                ", x23=" + x23 +
                ", x24=" + x24 +
                ", x25=" + x25 +
                ", x26=" + x26 +
                ", x27=" + x27 +
                ", x28=" + x28 +
                ", x29=" + x29 +
                ", x30=" + x30 +
                ", department=" + department +
                '}';
    }
}
