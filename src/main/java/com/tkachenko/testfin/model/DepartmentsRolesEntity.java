package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "departments_roles", schema = "mydb")
public class DepartmentsRolesEntity {
    private int departmentRoleId;
    private String role;
    private DepartmentEntity departmentEntity;

    @ManyToOne
    @JoinColumn(name = "login", nullable = false)
    public DepartmentEntity getDepartmentEntity() {
        return departmentEntity;
    }

    public void setDepartmentEntity(DepartmentEntity departmentEntity) {
        this.departmentEntity = departmentEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_role_id")
    public int getDepartmentRoleId() {
        return departmentRoleId;
    }

    public void setDepartmentRoleId(int departmentRoleId) {
        this.departmentRoleId = departmentRoleId;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentsRolesEntity that = (DepartmentsRolesEntity) o;

        if (departmentRoleId != that.departmentRoleId) return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = departmentRoleId;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
