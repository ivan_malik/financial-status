package com.tkachenko.testfin.model;

import javax.persistence.*;

@Entity
@Table(name = "financial_stability_group", schema = "mydb")
public class FinancialStabilityGroupEntity {
    private int financialStabilityGroupId;
    private Double y4;
    private Double y5;
    private Double y6;
    private Double y7;
    private Double y8;
    private Double z2;
    private RecvalFsgEntity recvalFsgEntity;
    private IndicatorsEntity indicatorsEntity;

    @ManyToOne
    @JoinColumn(name = "indicators_id", nullable = false)
    public IndicatorsEntity getIndicatorsEntity() {
        return indicatorsEntity;
    }

    public void setIndicatorsEntity(IndicatorsEntity indicatorsEntity) {
        this.indicatorsEntity = indicatorsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "recommend_values", nullable = false)
    public RecvalFsgEntity getRecvalFsgEntity() {
        return recvalFsgEntity;
    }

    public void setRecvalFsgEntity(RecvalFsgEntity recvalFsgEntity) {
        this.recvalFsgEntity = recvalFsgEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "financial_stability_group_id")
    public int getFinancialStabilityGroupId() {
        return financialStabilityGroupId;
    }

    public void setFinancialStabilityGroupId(int financialStabilityGroupId) {
        this.financialStabilityGroupId = financialStabilityGroupId;
    }

    @Basic
    @Column(name = "y4")
    public Double getY4() {
        return y4;
    }

    public void setY4(Double y4) {
        this.y4 = y4;
    }

    @Basic
    @Column(name = "y5")
    public Double getY5() {
        return y5;
    }

    public void setY5(Double y5) {
        this.y5 = y5;
    }

    @Basic
    @Column(name = "y6")
    public Double getY6() {
        return y6;
    }

    public void setY6(Double y6) {
        this.y6 = y6;
    }

    @Basic
    @Column(name = "y7")
    public Double getY7() {
        return y7;
    }

    public void setY7(Double y7) {
        this.y7 = y7;
    }

    @Basic
    @Column(name = "y8")
    public Double getY8() {
        return y8;
    }

    public void setY8(Double y8) {
        this.y8 = y8;
    }

    @Basic
    @Column(name = "z2")
    public Double getZ2() {
        return z2;
    }

    public void setZ2(Double z2) {
        this.z2 = z2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialStabilityGroupEntity that = (FinancialStabilityGroupEntity) o;

        if (financialStabilityGroupId != that.financialStabilityGroupId) return false;
        if (y4 != null ? !y4.equals(that.y4) : that.y4 != null) return false;
        if (y5 != null ? !y5.equals(that.y5) : that.y5 != null) return false;
        if (y6 != null ? !y6.equals(that.y6) : that.y6 != null) return false;
        if (y7 != null ? !y7.equals(that.y7) : that.y7 != null) return false;
        if (y8 != null ? !y8.equals(that.y8) : that.y8 != null) return false;
        if (z2 != null ? !z2.equals(that.z2) : that.z2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = financialStabilityGroupId;
        result = 31 * result + (y4 != null ? y4.hashCode() : 0);
        result = 31 * result + (y5 != null ? y5.hashCode() : 0);
        result = 31 * result + (y6 != null ? y6.hashCode() : 0);
        result = 31 * result + (y7 != null ? y7.hashCode() : 0);
        result = 31 * result + (y8 != null ? y8.hashCode() : 0);
        result = 31 * result + (z2 != null ? z2.hashCode() : 0);
        return result;
    }
}
