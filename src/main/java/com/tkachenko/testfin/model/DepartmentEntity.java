package com.tkachenko.testfin.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "department", schema = "mydb")
public class DepartmentEntity {
    private String login;
    private String password;
    private String title;
    private byte enabled;
    private CityEntity cityEntity;
    private Set<DepartmentsRolesEntity> departmentsRolesEntities = new HashSet<>();
    private Set<IndicatorsEntity> indicatorsEntities = new HashSet<>();

    @OneToMany(mappedBy = "department", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<IndicatorsEntity> getIndicatorsEntities() {
        return indicatorsEntities;
    }

    public void setIndicatorsEntities(Set<IndicatorsEntity> indicatorsEntities) {
        this.indicatorsEntities = indicatorsEntities;
    }

    @OneToMany(mappedBy = "departmentEntity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Set<DepartmentsRolesEntity> getDepartmentsRolesEntities() {
        return departmentsRolesEntities;
    }

    public void setDepartmentsRolesEntities(Set<DepartmentsRolesEntity> departmentsRolesEntities) {
        this.departmentsRolesEntities = departmentsRolesEntities;
    }

    @ManyToOne
    @JoinColumn(name = "city_id", nullable = false)
    public CityEntity getCityEntity() {
        return cityEntity;
    }

    public void setCityEntity(CityEntity cityEntity) {
        this.cityEntity = cityEntity;
    }

    @Id
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "enabled")
    public byte getEnabled() {
        return enabled;
    }

    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    public DepartmentEntity(String login, String password, String title, byte enabled, CityEntity cityEntity) {
        this.login = login;
        this.password = password;
        this.title = title;
        this.enabled = enabled;
        this.cityEntity = cityEntity;
    }

    public DepartmentEntity() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentEntity that = (DepartmentEntity) o;

        if (enabled != that.enabled) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (int) enabled;
        return result;
    }

    @Override
    public String toString() {
        return "DepartmentEntity{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", title='" + title + '\'' +
                ", enabled=" + enabled +
                ", cityEntity=" + cityEntity +
                '}';
    }
}
