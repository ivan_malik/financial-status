package com.tkachenko.testfin.fuzzy;

import java.util.ArrayList;
import java.util.List;

public class ClassicMethod {

    private FinancialCoefficients financialCoefficients;
    private List<Integer> ratings;

    public ClassicMethod(FinancialCoefficients financialCoefficients) {
        this.financialCoefficients = financialCoefficients;
        ratings = new ArrayList<>();
    }

    public String getStringResult() {
        double evaluation = -1;
        scaling();
        if (ratings.size() > 0)
            evaluation = getDoubleResult();
        else
            System.out.println("PIZDA");

        if (evaluation <= 3 && evaluation > 2.6)
            return "EXCELLENT";
        else if (evaluation <= 2.6 && evaluation > 2)
            return "GOOD";
        else if (evaluation <= 2 && evaluation > 1.2)
            return "NORMAL";
        else if (evaluation <= 1.2 && evaluation > 0.6)
            return "BAD";
        else if (evaluation <= 0.6 && evaluation >= 0)
            return "CRITICAL";
        else return "FUCKING SHIT";
    }

    private void scaling() {
        ratings.add(evaluation(financialCoefficients.getY1(), 2));
        ratings.add(evaluation(financialCoefficients.getY2(), 1));
        ratings.add(evaluation(financialCoefficients.getY3(), 0.2));

        ratings.add(evaluation(financialCoefficients.getY4(), 0.1));
        ratings.add(evaluation(financialCoefficients.getY5(), 0.7));
        ratings.add(evaluation(financialCoefficients.getY6(), 0.5));
        ratings.add(backwardEvaluation(financialCoefficients.getY7(), 0.8));
        ratings.add(evaluation(financialCoefficients.getY8(), 0.7));

        ratings.add(evaluation(financialCoefficients.getY9(), 0.3));
        ratings.add(evaluation(financialCoefficients.getY10(), 0.2));
        ratings.add(evaluation(financialCoefficients.getY11(), 0.25));
        ratings.add(evaluation(financialCoefficients.getY12(), 0.1));

        ratings.add(evaluation(financialCoefficients.getY13(), 7.5));
        ratings.add(evaluation(financialCoefficients.getY14(), 5));
        ratings.add(evaluation(financialCoefficients.getY15(), 4.5));
        ratings.add(evaluation(financialCoefficients.getY16(), 8));
    }

    private int evaluation(double value, double recommendValue) {
        //if recommendValue = 6
        double a = recommendValue - (recommendValue/4.0);//4.5
        double a2 = recommendValue - 2*(recommendValue/4.0);//3
        double a3 = recommendValue - 3*(recommendValue/4.0);//1.5

        if (value >= a )
            return 3;
        else if (value >= a2 &&  value < a ) {
            return 2;
        } else if ( value >= a3 && value < a2 ){
            return 1;
        } else
            return 0;
    }

    private int backwardEvaluation(double value, double unrecommendValue) {
        if (value >= unrecommendValue )
            return 0;
        else if ( (value < unrecommendValue) && ( value >= (unrecommendValue - (unrecommendValue/3.0)) ) ) {
            return 1;
        } else if (( value < (unrecommendValue - (unrecommendValue/3.0))) && (value >= (unrecommendValue - (2*unrecommendValue/3.0))) ){
            return 2;
        } else
            return 3;
    }

    public double getDoubleResult() {
        scaling();
        double C1 = (ratings.get(0) + ratings.get(1) + ratings.get(2)) / 3.0;
        //System.out.print(C1 + " ");
        double C2 = (ratings.get(3) + ratings.get(4) + ratings.get(5) + ratings.get(6) + ratings.get(7) ) / 5.0;
        //System.out.print(C2 + " ");
        double C3 = (ratings.get(8) + ratings.get(9) + ratings.get(10) + ratings.get(11)) / 4.0;
        //System.out.print(C3 + " ");
        double C4 =  (ratings.get(12) + ratings.get(13) + ratings.get(14) + ratings.get(15)) / 4.0;
        //System.out.println(C4 + " ");
        //System.out.println((C1 * 0.3) + (C2 * 0.25) + (C3 * 0.2) + (C4 * 0.25));
        return (C1 * 0.3) + (C2 * 0.25) + (C3 * 0.2) + (C4 * 0.25);
    }

    public void printRatings() {
        for (int i = 0; i < ratings.size(); i++) {
            System.out.print(ratings.get(i) + " ");
            if (i == 2)
                System.out.println();
            if (i == 7)
                System.out.println();
            if (i == 11)
                System.out.println();

        }
        System.out.println();
    }



}
