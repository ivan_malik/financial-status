package com.tkachenko.testfin.fuzzy;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.rule.Variable;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FuzzyMethod {

    private FinancialCoefficients financialCoefficients;

    public FuzzyMethod(FinancialCoefficients financialCoefficients) {
        this.financialCoefficients = financialCoefficients;
    }

    public double getDoubleResult() throws IOException {
        double z1 = findZ1(financialCoefficients.getY1(), financialCoefficients.getY2(), financialCoefficients.getY3());
        System.out.println("z1 = " + z1);
        double z2 = findZ2(financialCoefficients.getY4(), financialCoefficients.getY5(), financialCoefficients.getY6(),
                financialCoefficients.getY7(), financialCoefficients.getY8());
        System.out.println("z2 = " + z2);
        double z3 = findZ3(financialCoefficients.getY9(), financialCoefficients.getY10(), financialCoefficients.getY11(),
                financialCoefficients.getY12());
        System.out.println("z3 = " + z3);
        double z4 = findZ4(financialCoefficients.getY13(), financialCoefficients.getY14(), financialCoefficients.getY15(),
                financialCoefficients.getY16());
        System.out.println("z4 = " + z4);

        double z14 = findZ14(z1, z2, z3, z4);
        System.out.println("z14 = " + z14);

        double y17 = findY17(financialCoefficients.getIndicators().get(26), financialCoefficients.getIndicators().get(27));
        System.out.println("y17 = " + y17);
        double y18 = findY18(financialCoefficients.getIndicators().get(28), financialCoefficients.getIndicators().get(29));
        System.out.println("y18 = " + y18);
        double z5 = findZ5(y17, y18);
        System.out.println("z5 = " + z5);


        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("z14", z14);
        inputs.put("z5", z5);

        File file = ResourceUtils.getFile("classpath:Yfin.fcl");
        return findFuzzyValue(inputs, "yfin", file.getPath());
    }

    public String getStringResult() throws IOException {
        double result = getDoubleResult();
        if (result <= 5 && result > 4.35)
            return "EXCELLENT";
        else if (result <= 4.35 && result > 3.35)
            return "GOOD";
        else if (result <= 3.35 && result > 2)
            return "NORMAL";
        else if (result <= 2 && result > 1)
            return "BAD";
        else if (result <= 1 && result >= 0)
            return "CRITICAL";
        else return "FUCKING SHIT";

    }

    public static double findZ1(double y1, double y2, double y3) throws IOException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("y1", y1);
        inputs.put("y2", y2);
        inputs.put("y3", y3);

        File file = ResourceUtils.getFile("classpath:z1.fcl");
        return findFuzzyValue(inputs, "z1", file.getPath());
    }

    public static double findZ2(double y4, double y5, double y6, double y7, double y8) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("y4", y4);
        inputs.put("y5", y5);
        inputs.put("y6", y6);
        inputs.put("y7", y7);
        inputs.put("y8", y8);

        File file = ResourceUtils.getFile("classpath:z2.fcl");
        return findFuzzyValue(inputs, "z2", file.getPath());
    }

    public static double findZ3(double y9, double y10, double y11, double y12) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("y9", y9);
        inputs.put("y10", y10);
        inputs.put("y11", y11);
        inputs.put("y12", y12);

        File file = ResourceUtils.getFile("classpath:z3.fcl");
        return findFuzzyValue(inputs, "z3", file.getPath());
    }

    public static double findZ4(double y13, double y14, double y15, double y16) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("y13", y13);
        inputs.put("y14", y14);
        inputs.put("y15", y15);
        inputs.put("y16", y16);

        File file = ResourceUtils.getFile("classpath:z4.fcl");
        return findFuzzyValue(inputs, "z4", file.getPath());
    }

    public static double findZ14(double z1, double z2, double z3, double z4) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("z1", z1);
        inputs.put("z2", z2);
        inputs.put("z3", z3);
        inputs.put("z4", z4);

        File file = ResourceUtils.getFile("classpath:z14.fcl");
        return findFuzzyValue(inputs, "z14", file.getPath());
    }

    public static double findY17(double x27, double x28) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("x27", x27);
        inputs.put("x28", x28);

        File file = ResourceUtils.getFile("classpath:y17.fcl");
        return findFuzzyValue(inputs, "y17", file.getPath());
    }

    public static double findY18(double x29, double x30) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("x29", x29);
        inputs.put("x30", x30);

        File file = ResourceUtils.getFile("classpath:y18.fcl");
        return findFuzzyValue(inputs, "y18", file.getPath());
    }

    public static double findZ5(double y17, double y18) throws FileNotFoundException {
        HashMap<String, Double> inputs = new HashMap<>();
        inputs.put("y17", y17);
        inputs.put("y18", y18);

        File file = ResourceUtils.getFile("classpath:z5.fcl");
        return findFuzzyValue(inputs, "z5", file.getPath());
    }


    private static double findFuzzyValue(HashMap<String, Double> inputs, String resultName, String fileName) throws FileNotFoundException {

        FIS fis = FIS.load(fileName,true);
        if( fis == null ) {
            System.err.println("Can't load file: '" + fileName + "'");
            throw new FileNotFoundException();
        }

        FunctionBlock functionBlock = fis.getFunctionBlock(null);

        for (Map.Entry<String, Double> entry : inputs.entrySet()) {
            fis.setVariable(entry.getKey() , entry.getValue());
        }

        fis.evaluate();

        Variable result = functionBlock.getVariable(resultName);
        result.getDefuzzifier().defuzzify();

        return result.getValue();
    }
}
