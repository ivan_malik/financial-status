package com.tkachenko.testfin.fuzzy;


import java.util.ArrayList;

public class FinancialCoefficients {

    private ArrayList<Double> indicators;

    public FinancialCoefficients(ArrayList<Double> indicators) {
        this.indicators = indicators;
    }

    public ArrayList<Double> getIndicators() {
        return indicators;
    }

    //liquidity
    public double getY1() {
        double y1 = (indicators.get(5) / (indicators.get(2) + indicators.get(3) + indicators.get(4)));
        return y1;
    }

    public double getY2() {
        double y2 = ((indicators.get(0) + indicators.get(1) + indicators.get(14)) / (indicators.get(2) + indicators.get(3) + indicators.get(4)));
        return y2;
    }

    public double getY3() {
        double y3 = ((indicators.get(0) + indicators.get(1)) / (indicators.get(2) + indicators.get(3) + indicators.get(4)));
        return y3;
    }

    //financial stability
    public double getY4() {
        double y4 = ((indicators.get(8) - indicators.get(7)) / (indicators.get(5)));
        return y4;
    }

    public double getY5() {
        double y5 = ((indicators.get(8) + indicators.get(16) + indicators.get(17)) / (indicators.get(10)));
        return y5;
    }

    public double getY6() {
        double y6 = ((indicators.get(8) - indicators.get(7)) / indicators.get(8));
        return y6;
    }

    public double getY7() {
        double y7 = ((indicators.get(12) + indicators.get(13) - indicators.get(15) + indicators.get(16) + indicators.get(17)) / (indicators.get(18)));
        return y7;
    }

    public double getY8() {
        double y8 = ((indicators.get(12) + indicators.get(13)) / (indicators.get(8)));
        return y8;
    }

    //profitability
    public double getY9() {
        double y9 = (indicators.get(19) / (indicators.get(20) * 100));
        return y9;
    }

    public double getY10() {
        double y10 = (indicators.get(21) / (indicators.get(10) * 100));
        return y10;
    }

    public double getY11() {
        double y11 = (indicators.get(19) / (0.5 * indicators.get(8) * 100));
        return y11;
    }

    public double getY12() {
        double y12 = (indicators.get(22) / (indicators.get(20) * 100));
        return y12;
    }

    //business activity
    public double getY13() {
        double y13 = (indicators.get(20) / ((indicators.get(10) + indicators.get(11)) * 0.5));
        return y13;
    }

    public double getY14() {
        double y14 = (indicators.get(20) / ((indicators.get(5) + indicators.get(6)) * 0.5));
        return y14;
    }

    public double getY15() {
        double y15 = (indicators.get(20) / ((indicators.get(8) + indicators.get(9)) * 0.5));
        return y15;
    }

    public double getY16() {
        double y16 = (indicators.get(25) / ((indicators.get(23) + indicators.get(24)) * 0.5));
        return y16;
    }

    public void print() {
        System.out.println("y1 = " + getY1());
        System.out.println("y2 = " + getY2());
        System.out.println("y3 = " + getY3());
        System.out.println("y4 = " + getY4());
        System.out.println("y5 = " + getY5());
        System.out.println("y6 = " + getY6());
        System.out.println("y7 = " + getY7());
        System.out.println("y8 = " + getY8());
        System.out.println("y9 = " + getY9());
        System.out.println("y10 = " + getY10());
        System.out.println("y11 = " + getY11());
        System.out.println("y12 = " + getY12());
        System.out.println("y13 = " + getY13());
        System.out.println("y14 = " + getY14());
        System.out.println("y15 = " + getY15());
        System.out.println("y16 = " + getY16());

    }
}
