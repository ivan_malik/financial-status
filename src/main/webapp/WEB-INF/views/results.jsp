<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>

<head>
    <title>История состояний</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<c:url value="/resourses/css/bootstrap.min.css" />" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="<c:url value="/resourses/js/bootstrap.min.js" />"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="${pageContext.request.contextPath}/"><img src="<c:url value="/resourses/img/logo2.png" />" width="93" height="30.5"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Просмотр истории рассчетов </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Справка</a>
            </li>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/admin">Страница администратора</a>
                </li>
            </sec:authorize>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>

<div class="card">
    <div class="card-header">
        <h5>Первая группа</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">y1</th>
                <th scope="col">y2</th>
                <th scope="col">y3</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${coefficients.y1}</td>
                <td>${coefficients.y2}</td>
                <td>${coefficients.y3}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-header">
        <h5>Вторая группа</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">y4</th>
                <th scope="col">y5</th>
                <th scope="col">y6</th>
                <th scope="col">y7</th>
                <th scope="col">y8</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${coefficients.y4}</td>
                <td>${coefficients.y5}</td>
                <td>${coefficients.y6}</td>
                <td>${coefficients.y7}</td>
                <td>${coefficients.y8}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-header">
        <h5>Третья группа</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">y9</th>
                <th scope="col">y10</th>
                <th scope="col">y11</th>
                <th scope="col">y12</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${coefficients.y9}</td>
                <td>${coefficients.y10}</td>
                <td>${coefficients.y11}</td>
                <td>${coefficients.y12}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-header">
        <h5>Четвертая группа</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">y13</th>
                <th scope="col">y14</th>
                <th scope="col">y15</th>
                <th scope="col">y16</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${coefficients.y13}</td>
                <td>${coefficients.y14}</td>
                <td>${coefficients.y15}</td>
                <td>${coefficients.y16}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-header">
        <h5>Качественная группа</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">y17</th>
                <th scope="col">y18</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${y17}</td>
                <td>${y18}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-header">
        <h5>Результаты состояния</h5>
    </div>
    <div class="card card-body">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">z1</th>
                <th scope="col">z2</th>
                <th scope="col">z3</th>
                <th scope="col">z4</th>
                <th scope="col">z5</th>
                <th scope="col">Classic результат</th>
                <th scope="col">Результат</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${z1}</td>
                <td>${z2}</td>
                <td>${z3}</td>
                <td>${z4}</td>
                <td>${z5}</td>
                <td>${stringClassic}&nbsp;(${doubleClassic}/3)</td>
                <td>${stringFuzzy}&nbsp;(${doubleFuzzy}/5)</td>
            </tr>
            </tbody>
        </table>
    </div>
    <a href="<c:url value='/download' />" class="btn btn-outline-info my-2 my-sm-0" >Получить отчет</a>
</div>

</body>
</html>
