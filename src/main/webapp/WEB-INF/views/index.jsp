<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML>
<html>

<head>
    <title>Добро пожаловать</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/resourses/css/bootstrap.min.css" />" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="<c:url value="/resourses/js/bootstrap.min.js" />"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="${pageContext.request.contextPath}/"><img src= "<c:url value="/resourses/img/logo2.png" />" width="93" height="30.5"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/history">Просмотр истории рассчетов </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/info">Справка</a>
            </li>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/admin">Страница администратора</a>
            </li>
            </sec:authorize>
        </ul>
        <c:url value="/j_spring_security_logout" var="logoutUrl" />
        <form  action="${logoutUrl}" method="post" id="logoutForm">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2>
                <a class="btn btn-outline-success my-2 my-sm-0" href="javascript:formSubmit()"> Logout</a>
            </h2>
        </c:if>
    </div>
</nav>
<div class="card">
    <div class="card-header">
        <h3>Выполнение рассчетов</h3>
    </div>
    <div class="card-body">
        <p>
            <a class="btn btn-primary " data-toggle="collapse" href="#multiCollapseExample1" role="button"
               aria-expanded="false" aria-controls="multiCollapseExample1">Загрузить с Excel файла</a>
            <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample2" role="button"
               aria-expanded="false" aria-controls="multiCollapseExample2">Загрузить с базы данных</a>
        </p>
        <div class="row ">
            <div class="col-6">
                <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body">
                        <form action="${pageContext.request.contextPath}/uploadExcel" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="file" name="file" accept=".xls,.xlsx" class="custom-file-input" id="customFile">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                <label class="custom-file-label" for="customFile">Выберите файл</label>
                                <input class="btn btn-primary" type="submit" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="collapse multi-collapse" id="multiCollapseExample2">
                    <div class="card card-body">
                        <form action="${pageContext.request.contextPath}/fromdb" method="get">
                            <div class="form-group">
                                <select size="1" class="form-control" id="exampleFormControlSelect1" name="indicator">
                                    <c:forEach var="indicator" items="${allIndicators}" varStatus="status">
                                        <option value="${indicator.idindicatorsId}">${indicator.calculationDate} at ${indicator.calculationTime}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Отправить">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card card-body">
        <form:form action="${pageContext.request.contextPath}/perform" method="get" modelAttribute="indicators">
            <div class="card">
                <div class="card-header">
                    <h5>Введите параметры</h5>
                </div>
                <div class="card card-body">
                    <div class="form-row">
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault01">x1</label>
                            <form:input path="x1" class="form-control" id="validationDefault01" name="x1" type="number" min="0" step="any"
                                   placeholder="0.0"  required="required" value="${indicators.x1}"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault02">x2</label>
                            <form:input path="x2" type="number" class="form-control" name="x2" id="validationDefault02" placeholder="0.0" min="0" step="any"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault03">x3</label>
                            <form:input path="x3" type="number" class="form-control" id="validationDefault03" placeholder="0.0" name="x3" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault04">x4</label>
                            <form:input path="x4" type="number" class="form-control" id="validationDefault04" placeholder="0.0" name="x4" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault05">x5</label>
                            <form:input path="x5" type="number" class="form-control" id="validationDefault05" placeholder="0.0" name="x5" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault06">x6</label>
                            <form:input path="x6" type="number" class="form-control" id="validationDefault06" placeholder="0.0" name="x6" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault07">x7</label>
                            <form:input path="x7" type="number" class="form-control" id="validationDefault07" placeholder="0.0" name="x7" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault08">x8</label>
                            <form:input path="x8" type="number" class="form-control" id="validationDefault08" placeholder="0.0" name="x8" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault09">x9</label>
                            <form:input path="x9" type="number" class="form-control" id="validationDefault09" placeholder="0.0" name="x9" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault10">x10</label>
                            <form:input path="x10" type="number" class="form-control" id="validationDefault10" placeholder="0.0" name="x10" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault11">x11</label>
                            <form:input path="x11" type="number" class="form-control" id="validationDefault11" placeholder="0.0" name="x11" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault12">x12</label>
                            <form:input path="x12" type="number" class="form-control" id="validationDefault12" placeholder="0.0" name="x12" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault13">x13</label>
                            <form:input path="x13" type="number" class="form-control" id="validationDefault13" placeholder="0.0" name="x13" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault14">x14</label>
                            <form:input path="x14" type="number" class="form-control" id="validationDefault14" placeholder="0.0" name="x14" min="0"
                                   required="requird"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault15">x15</label>
                            <form:input path="x15" type="number" class="form-control" id="validationDefault15" placeholder="0.0" name="x15" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault16">x16</label>
                            <form:input path="x16" type="number" class="form-control" id="validationDefault16" placeholder="0.0" name="x16" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault17">x17</label>
                            <form:input path="x17" type="number" class="form-control" id="validationDefault17" placeholder="0.0" name="x17" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault18">x18</label>
                            <form:input path="x18" type="number" class="form-control" id="validationDefault18" placeholder="0.0" name="x18" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault19">x19</label>
                            <form:input path="x19" type="number" class="form-control" id="validationDefault19" placeholder="0.0" name="x19" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault20">x20</label>
                            <form:input path="x20" type="number" class="form-control" id="validationDefault20" placeholder="0.0" name="x20" min="0"
                                 required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault21">x21</label>
                            <form:input path="x21" type="number" class="form-control" id="validationDefault21" placeholder="0.0" name="x21" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault22">x22</label>
                            <form:input path="x22" type="number" class="form-control" id="validationDefault22" placeholder="0.0" name="x22" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault23">x23</label>
                            <form:input path="x23" type="number" class="form-control" id="validationDefault23" placeholder="0.0" name="x23" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault24">x24</label>
                            <form:input path="x24" type="number" class="form-control" id="validationDefault24" placeholder="0.0" name="x24" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault25">x25</label>
                            <form:input path="x25" type="number" class="form-control" id="validationDefault25" placeholder="0.0" name="x25" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault26">x26</label>
                            <form:input path="x26" type="number" class="form-control" id="validationDefault26" placeholder="0.0" name="x26" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault27">x27</label>
                            <form:input path="x27" type="number" class="form-control" id="validationDefault27" placeholder="0.0" name="x27" min="0"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault28">x28</label>
                            <form:input path="x28" type="number" class="form-control" id="validationDefault28" placeholder="0.0" name="x28" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault29">x29</label>
                            <form:input path="x29" type="number" class="form-control" id="validationDefault29" placeholder="0.0" name="x29" min="0"
                                   required="required"/>
                        </div>
                        <div class="col-md-2 mb-2">
                            <label for="validationDefault30">x30</label>
                            <form:input path="x30" type="number" class="form-control" id="validationDefault30" placeholder="0.0" name="x30" min="0"
                                 required="required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Рассчитать</button>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>
<script>
    function formSubmit() {
        document.getElementById("logoutForm").submit();
    }
</script>
</body>

</html>
