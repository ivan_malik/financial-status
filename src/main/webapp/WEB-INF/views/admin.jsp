<%@page session="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin 2 - Dashboard</title>
    <link rel="stylesheet" href="<c:url value="/resourses/vendor/fontawesome-free/css/all.min.css" />" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="<c:url value="/resourses/css/sb-admin-2.min.css" />" />

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Admin</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">



        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Управление</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Управление чем нибудь:</h6>
                    <a class="collapse-item" href="#">Филиалы</a>
                    <a class="collapse-item" href="#">Параметры</a>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Addons
        </div>


        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>История состояний</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-table"></i>
                <span>Tables</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                    <a href="${pageContext.request.contextPath}/" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Вернуться</a>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>История рассчетов филиалов</h5>
                    </div>
                    <div class="card card-body">
                        <form>
                            <div class="form-row ">
                                <div class="form-group">
                                    <select size="1" class="form-control" id="exampleFormControlSelect1">
                                        <option disabled selected>Выберите отделение</option>
                                        <option value="1">выавы</option>
                                        <option value="2">выаыва</option>
                                        <option value="3">цукуц</option>
                                        <option value="4">ясмисч</option>
                                        <option value="5">ыфыф</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Результаты рассчетов</h5>
                    </div>
                    <div class="card card-body">
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Дата</th>
                                <th scope="col">y1</th>
                                <th scope="col">y2</th>
                                <th scope="col">y3</th>
                                <th scope="col">y4</th>
                                <th scope="col">y5</th>
                                <th scope="col">z1</th>
                                <th scope="col">z2</th>
                                <th scope="col">z3</th>
                                <th scope="col">z4</th>
                                <th scope="col">z5</th>
                                <th scope="col">Результат</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>10.12.2019</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>Хорошо</td>
                            </tr>
                            <tr>
                                <td>10.12.2019</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>Хорошо</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card card-body">
                        <h3 class="text-center">График изменения сосотояния</h3>
                    </div>
                </div>
                <script>
                    window.onload = function () {

                        var chart = new CanvasJS.Chart("chartContainer", {
                            animationEnabled: true,
                            theme: "light2",
                            title:{
                                text: "Храктеристика филиала"
                            },
                            axisX:{

                                valueFormatString: "DD MMM",
                                crosshair: {
                                    enabled: true,
                                    snapToDataPoint: true
                                }
                            },
                            axisY: {
                                title: "Состояние",

                            },
                            toolTip:{
                                shared:true
                            },
                            legend:{
                                cursor:"pointer",
                                verticalAlign: "bottom",
                                horizontalAlign: "left",
                                dockInsidePlotArea: true,
                                itemclick: toogleDataSeries
                            },
                            data: [{
                                type: "line",
                                showInLegend: true,
                                name: "Состояние",
                                markerType: "square",
                                xValueFormatString: "DD MMM, YYYY",
                                color: "#F08080",
                                dataPoints: [
                                    { x: new Date(2017, 0, 12), y: 1, indexLabel: "плохо", markerType: "cross", markerColor: "red" },
                                    { x: new Date(2017, 6, 12), y: 1, indexLabel: "очень плохо", markerType: "cross", markerColor: "red" },
                                    { x: new Date(2017, 10, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                                    { x: new Date(2018, 0, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                                    { x: new Date(2018, 1, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato" },
                                    { x: new Date(2018, 2, 12), y: 1, indexLabel: "очень плохо", markerType: "cross", markerColor: "red" },
                                    { x: new Date(2018, 4, 12), y: 2, indexLabel: "плохо", markerType: "cross", markerColor: "tomato"},
                                    { x: new Date(2018, 5, 15), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                                    { x: new Date(2018, 6, 16), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                                    { x: new Date(2018, 7, 13), y: 4, indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                                    { x: new Date(2018, 8, 11), y: 3, indexLabel: "средне", markerType: "circle", markerColor: "gray" },
                                    { x: new Date(2018, 10, 12), y: 4, indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                                    { x: new Date(2019, 0, 16), y: 4,  indexLabel: "хорошо", markerType: "triangle", markerColor: "blue" },
                                    { x: new Date(2019, 2, 11), y: 5,  indexLabel: "очень хорошо", markerType: "triangle", markerColor: "green" },
                                    { x: new Date(2019, 3, 11), y: ""}

                                ]
                            }]
                        });
                        chart.render();

                        function toogleDataSeries(e){
                            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                e.dataSeries.visible = false;
                            } else{
                                e.dataSeries.visible = true;
                            }
                            chart.render();
                        }

                    }
                </script>

                <div id="chartContainer" style="height: 500px; max-width: 1280px; margin: 0px auto;"></div>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                <!-- Bootstrap core JavaScript-->
                <script src="<c:url value="/resourses/vendor/jquery/jquery.min.js"/>"></script>
                <script src="<c:url value="/resourses/vendor/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

                <!-- Core plugin JavaScript-->
                <script src="<c:url value="/resourses/vendor/jquery-easing/jquery.easing.min.js" />"></script>


                <script src="<c:url value="/resourses/js/sb-admin-2.min.js" />"></script>
            </div>
        </div>
    </div>
</div>
</body>

</html>

